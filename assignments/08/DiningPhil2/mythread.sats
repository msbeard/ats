(* ****** ****** *)
//
// HX-2013-10-26:
// Some basic thread-related operations
//
(* ****** ****** *)

%{#
#include <pthread.h>
%} // end of [%{#]

(* ****** ****** *)
//
fun mythread_create_funenv
  {env:viewtype} (fwork: (env) -> void, env: env): void
//
fun mythread_create_cloptr (fwork: () -<lincloptr1> void): void
//
(* ****** ****** *)

absview mutex_v
abstype mutex_type = ptr
typedef mutex = mutex_type

(* ****** ****** *)

abstype condvar_type = ptr
typedef condvar = condvar_type

(* ****** ****** *)

fun mutex_create (): mutex

fun mutex_lock (m: mutex): (mutex_v | void)
fun mutex_trylock (m: mutex): [b:bool] (option_v(mutex_v, b) | bool(b))
fun mutex_unlock (pf: mutex_v | m: mutex): void

(* ****** ****** *)

fun condvar_create (): condvar

fun condvar_signal (cv: condvar): void
fun condvar_broadcast (cv: condvar): void

fun condvar_wait (pf: !mutex_v | cv: condvar, m: mutex): void

(* ****** ****** *)

(* end of [mythread.sats] *)
