(* ****** ****** *)
//
// MB-2013-10-31
//
// A straightforward implementation
// of the problem of Dining Philosophers
// with a washer
//
(* ****** ****** *)

%{#
#define NPHIL 5
%} // end of [%{#]
#define NPHIL 5

(* ****** ****** *)

typedef phil = int

(* ****** ****** *)

fun phil_left (n: phil): int
fun phil_right (n: phil): int

(* ****** ****** *)
//
absvt@ype
fork_vtype = int
//
vtypedef fork = fork_vtype
//
(* ****** ****** *)

fun randsleep (n: intGte(1)): void

(* ****** ****** *)

fun phil_acquire_lfork (n: phil): fork
fun phil_release_lfork (n: phil, f: fork): void

(* ****** ****** *)
// Picks up left and right fork from table of forks
fun phil_acquire_rfork (n: phil): fork

// Puts left and right fork in dirty tray
fun phil_release_rfork (n: phil, f: fork): void

(* ****** ****** *)

// Cleaner picks up fork in dirty tray
fun cleaner_acquire_fork () : fork

// Cleaner puts clean fork on table 
fun cleaner_release_fork (f: fork) : void

(* ****** ****** *)

fun phil_dine (n: phil): void
fun phil_think (n: phil): void

(* ****** ****** *)

fun cleaner_wash (): void

(* ****** ****** *)

fun phil_loop (n: phil): void

(* ****** ****** *)

fun cleaner_loop ((*void*)): void

(* ****** ****** *)

(* end of [DiningPhil2.sats] *)
