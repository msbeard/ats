//
// The 5th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 10th of October, 2013
//
(* ****** ****** *)
//
// You are only require to solve *one* of the following
// two problems. If you do both, you will get bonus points
// for the second one.
//
(* ****** ****** *)
//
// HX: 30 points
// Please implement the Euler transform:
// Given a sequence:
//   x(0), x(1), x(2), ..., x(n), ...
// We can form another sequence:
//   y(0), y(1), y(2), ..., y(n), ....
// such that each y(n) equals the the follow value 
//
// x(n+2) - (x(n+2)-x(n+1))^2 / (x(n)+x(n+2)-2*x(n+1))
//
fun EulerTrans (xs: stream (double)): stream (double)

(* ****** ****** *)
//
typedef int2 = @(int, int)
//
(* ****** ****** *)
//
// The following function enumerates all the pairs of natural numbers
// (i, j) satisfying i <= j:
//
fun intpair_enumerate (): stream (int2) // HX: its implementation is given
//
(* ****** ****** *)
//
// HX: 30 bonus points
//
// Given a function f that is monotone at both of its arguments
// (that is, f (i1, j1) <= f(i2, j2) whenever i1 <= i2 and j1 <= j2),
// the following function enumerates all the pairs of natural numbers
// (i, j) satisfying i <= j such that (i1, j1) appears ahead of (i2, j2)
// if and only if
//
// (1) f(i1, j1) < f(i2, j2) or
// (2) f(i1, j1) = f(i2, j2) and i1 < i2
//
fun intpair_enumerate_with_ordering (f: (int, int) -<fun0> int): stream (int2)
//
fun ismonotone (f: (int, int) -<fun0> int, x: int2, y: int2) : bool
(* ****** ****** *)

(* end of [assignment5.sats] *)
