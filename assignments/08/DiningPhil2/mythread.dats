(* ****** ****** *)
//
// HX-2013-10-26:
// Some basic thread-related operations
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

staload
UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

staload "./mythread.sats"

(* ****** ****** *)

/*
int pthread_create
(
  pthread_t *thread
, const pthread_attr_t *attr
, void *(*start_routine) (void *)
, void *arg
) ;
*/

abst@ype pthread_t = $extype"pthread_t"
abst@ype pthread_attr_t = $extype"pthread_attr_t"

implement
mythread_create_funenv
  (fwork, env) = let
//
var tid: pthread_t
var attr: pthread_attr_t
val err = $extfcall
  (int, "pthread_attr_init", addr@attr)
//
val err = $extfcall
(
  int
, "pthread_attr_setdetachstate"
, addr@attr, $extval(int, "PTHREAD_CREATE_DETACHED")
)
//
val err = $extfcall
(
  int
, "pthread_create"
, addr@tid, addr@attr, fwork, $UN.castvwtp0{ptr}(env)
)
//
val ((*void*)) = assertloc (err = 0)
//
val err = $extfcall (int, "pthread_attr_destroy", addr@attr)
//
in
  // nothing
end // end of [mythread_create_funenv]

(* ****** ****** *)

implement
mythread_create_cloptr (fwork) = let
//
fun app
(
  f: () -<lincloptr1> void
): void = let
  val () = f () in cloptr_free($UN.castvwtp0{cloptr0}(f))
end // end of [app]
//
in
  mythread_create_funenv (app, fwork)
end // end of [pthread_create_cloptr]

(* ****** ****** *)

assume mutex_v = unit_v

(* ****** ****** *)

implement
mutex_create () = let
//
typedef
pthread_mutex_t = $extype"pthread_mutex_t"
//
val () = println! ("Creating mutex")
val (pfat, pfgc | p) = ptr_alloc<pthread_mutex_t> ()
val err = $extfcall (int, "pthread_mutex_init", p, 0(*attr*))
val ((*void*)) = assertloc (err = 0) 
//
in
  $UN.castvwtp0{mutex}((pfat, pfgc | p))
end // end of [mutex_create]

(* ****** ****** *)

implement
mutex_lock (m) = let
//
val () = println! ("Trying to lock mutex")
val err = $extfcall (int, "pthread_mutex_lock", $UN.cast{ptr}(m))
val ((*void*)) = assertloc (err = 0)
//
in
  (unit_v () | ())
end // end of [mutex_lock]

(* ****** ****** *)

implement
mutex_unlock
  (pf | m) = let
//
prval unit_v () = pf
//
val err = $extfcall (int, "pthread_mutex_unlock", $UN.cast{ptr}(m))
val ((*void*)) = assertloc (err = 0)
//
in
  // nothing
end // end of [mutex_unlock]

(* ****** ****** *)

implement
condvar_create () = let
//
typedef
pthread_cond_t = $extype"pthread_cond_t"
//
val (pfat, pfgc | p) = ptr_alloc<pthread_cond_t> ()
val err = $extfcall (int, "pthread_cond_init", p, 0(*attr*))
val ((*void*)) = assertloc (err = 0) 
//
in
  $UN.castvwtp0{condvar}((pfat, pfgc | p))
end // end of [condvar_create]

(* ****** ****** *)

implement
condvar_signal (cv) = let
//
val err = $extfcall (int, "pthread_cond_signal", $UN.cast{ptr}(cv))
val ((*void*)) = assertloc (err = 0)
//
in
  // nothing
end // end of [condvar_signal]

(* ****** ****** *)

implement
condvar_broadcast (cv) = let
//
val err = $extfcall (int, "pthread_cond_broadcast", $UN.cast{ptr}(cv))
val ((*void*)) = assertloc (err = 0)
//
in
  // nothing
end // end of [condvar_broadcast]

(* ****** ****** *)

implement
condvar_wait
  (pf | cv, m) = let
//
val err = $extfcall (int, "pthread_cond_wait", $UN.cast{ptr}(cv), $UN.cast{ptr}(m))
val ((*void*)) = assertloc (err = 0)
//
in
  // nothing
end // end of [condvar_wait]

(* ****** ****** *)

(* end of [mythread.dats] *)
