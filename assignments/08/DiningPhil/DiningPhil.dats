(* ****** ****** *)
//
// HX-2013-10-26
//
// A straightforward implementation
// of the problem of Dining Philosophers
//
(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

staload
UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

staload "libc/SATS/stdlib.sats"
staload "libc/SATS/unistd.sats"

(* ****** ****** *)

staload "./mythread.sats"

(* ****** ****** *)

staload "./DiningPhil.sats"

(* ****** ****** *)

implement phil_left (n) = n
implement phil_right (n) = (n+1) mod NPHIL

(* ****** ****** *)

implement
randsleep (n) =
  ignoret (sleep($UN.cast{uInt}(rand() mod n + 1)))
// end of [randsleep]

(* ****** ****** *)

implement
phil_loop (n) = let
  val () = phil_think (n)
  val ((*void*)) = phil_dine (n)
in
  phil_loop (n)
end // end of [phil_loop]

(* ****** ****** *)
//
extern
fun phil_initiate (ntot: int): void
//
implement
phil_initiate
  (ntot) = let
//
fun loop (i: int): void =
(
  if i < ntot then let
    val () = println! ("phil_initiate: loop: i = ", i)
    val () = mythread_create_cloptr (llam () => phil_loop (i))
  in
    loop (i+1)
  end else ((*void*))
)
//
val () = loop (0)
//
in
  println! ("Initiation: ", ntot, " phils are added.")
end // end of [phil_initiate]

(* ****** ****** *)

dynload "./DiningPhil_fork.dats"

(* ****** ****** *)

implement
main0 ((*void*)) =
{
//
val () = phil_initiate (NPHIL)
//
val () = $extfcall (void, "pthread_exit", 0)
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [DiningPhil.dats] *)
