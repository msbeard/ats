(* ****** ****** *)
//
// MB-2013-10-31
//
// A straightforward implementation
// of the problem of Dining Philosophers
// with a washer
//
(* ****** ****** *)
//
#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"

(* ****** ****** *)

#define ATS_DYNLOADFLAG 1
// dynload "./DiningPhil_fork.dats"

(* ****** ****** *)

staload "./mythread.sats"
staload "./DiningPhil2.sats"
staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

assume fork_vtype = int

(* ****** ****** *)

// Philospher's methods
extern
fun fork_acquire (n: int): fork

extern
fun fork_acquire2 (pf: mutex_v | n: int): fork

extern
fun fork_release (n: int, f: fork): void

(* ****** ****** *)

// Washer's methods
extern 
fun dirty_fork_acquire (): fork

extern 
fun dirty_fork_acquire2 (pf: mutex_v | n: int): fork

extern
fun clean_fork_release (f: fork) : void

(* ****** ****** *)

extern
fun find_empty_slot (A: arrayref(int, NPHIL)) : int

extern 
fun find_non_empty_slot (A: arrayref(int, NPHIL)): int

(* ****** ****** *)

implement
phil_acquire_lfork (n) = f where
{
  val () = println! ("Phil(", n, ") trying to acquire left fork.")
  val f = fork_acquire (phil_left(n))
  val () = println! ("Phil(", n, ") acquires left fork.")
}

implement
phil_acquire_rfork (n) = f where
{
  val f = fork_acquire (phil_right(n))
  val () = println! ("Phil(", n, ") acquires right fork.")
}

(* ****** ****** *)

implement
phil_release_lfork (n, f) = 
{
  val () = fork_release (phil_left(n), f)
  val () = println! ("Phil(", n, ") releases left fork.")
}

implement
phil_release_rfork (n, f) =
{
  val () = fork_release (phil_right(n), f)
  val () = println! ("Phil(", n, ") releases right fork.")
}

(* ****** ****** *)

implement
cleaner_acquire_fork () = f where
{
  val () = println! ("Cleaner acquires dirty fork.")
  val f = dirty_fork_acquire ()
}

implement
cleaner_release_fork (f) =
{
  val () = clean_fork_release (f)
  val () = println! ("Cleaner releases fork.")
}

(* ****** ****** *)

%{^
//
int __the_forkarr[NPHIL] ;
int __the_dirty_forkarr[NPHIL] ;
//
ATSinline()
atstype_ptr
the_forkarr_get () { return &__the_forkarr[0] ; }

ATSinline()
atstype_ptr
the_dirty_forkarr_get () { return &__the_forkarr[0] ; }
%} // end of [%{^]

(* ****** ****** *)

local
//
extern
fun the_forkarr_get () : arrayref(int, NPHIL) = "ext#"
//
extern
fun the_dirty_forkarr_get () : arrayref(int, NPHIL) = "ext#"

//
fun forkarr_takeout
  (n: natLt(NPHIL)): int = f where
{
  val A = the_forkarr_get ()
  val f = A[n]
  val ((*void*)) = if f != 0 then A[n] := 0
}
fun dirty_forkarr_takeout
  (n: natLt(NPHIL)): int = f where
{
  val A = the_dirty_forkarr_get ()
  val f = A[n]
  val ((*void*)) = if f !=0 then A[n] := 0
}
fun forkarr_addback
  (n: natLt(NPHIL), f: int): void =
{
  val A = the_forkarr_get ()
  val ((*void*)) = A[n] := (f)
}
fun forkarr_addback2
  (f: int) : void = 
{
  val A = the_forkarr_get ()
  val n = find_empty_slot (A)
  val n = $UN.cast{natLt(NPHIL)}(n)
  val () = forkarr_addback (n, f)
}
fun dirty_forkarr_addback
  (n: natLt(NPHIL), f: int): void = 
{
  val A = the_dirty_forkarr_get ()
  val ((*void*)) = A[n] := (f)
}
//
val () = initloop (0) where
{
fun
initloop
  {i:nat} (i: int(i)): void =
  if i < NPHIL then 
	(
  	println! ("Initloop");
	forkarr_addback (i, 1(*f*)); 
	dirty_forkarr_addback (i, 0(*f*)); 
	initloop (i+1)
	) 
  else ()
// end of [initloop]
}
//
val M_forkarr = mutex_create ()
val M_dirty_forkarr = mutex_create ()
val CV_no_fork = condvar_create ()
val CV_dirty_fork = condvar_create ()
//
in (* in of [local] *)

(* ****** ****** *)

implement
fork_acquire (n) = let
//
val (pf | ()) = mutex_lock (M_forkarr)
//
in
  fork_acquire2 (pf | n)
end // end of [fork_acquire]

implement
fork_acquire2
  (pf | n) = let
//
val n =
  $UN.cast{natLt(NPHIL)}(n)
val f = forkarr_takeout (n)
//
in
//
if f != 0
  then let
    val () = mutex_unlock (pf | M_forkarr) in f
  end // end of [then]
  else let
    val () = condvar_wait (pf | CV_no_fork, M_forkarr) in fork_acquire2 (pf | n)
  end // end of [else]
// end of [if]
//
end // end of [fork_acquire2]

implement
dirty_fork_acquire () = let
  val () = println! ("Grabing mutex for dirty tray!")
  val (pf | ()) = mutex_lock (M_dirty_forkarr)
  val A = the_dirty_forkarr_get ()
  // val n = find_non_empty_slot (A)
  val n = 3
in
  dirty_fork_acquire2 (pf | n)
end // end of [dirty_fork_acquire]

implement
dirty_fork_acquire2 (pf | n) = let
  // Since I don't know the index of an element, I have to do a search to
  // find the first non-zero entry in the dirty fork tray
  val () = println! ("Getting dirty tray")
  // val A = the_dirty_forkarr_get ()
  // val n = find_non_empty_slot (A)
  val () = println! ("Slot = ", n)
  val n = $UN.cast{natLt(NPHIL)}(n)
  val f = dirty_forkarr_takeout (n)
in
  if f != 0
    then let
       val () = mutex_unlock (pf | M_dirty_forkarr) in f
    end // end of [then]
  else 
    let
      val () = condvar_wait (pf | CV_dirty_fork, M_dirty_forkarr) 
    in dirty_fork_acquire2 (pf | n)
    end // end of [else]
end // end of [dirty_fork_acquire2]

implement
fork_release (n, f) = let
//
  val n = $UN.cast{natLt(NPHIL)}(n)
  val (pf | ()) = mutex_lock (M_dirty_forkarr)
//
  val n = $UN.cast{natLt(NPHIL)}(n)
  val f = dirty_forkarr_addback (n, f)
//
  val  () = condvar_signal (CV_dirty_fork)
//
  val () = mutex_unlock (pf | M_dirty_forkarr)
//
in
  // nothing
end // end of [fork_release]

implement 
clean_fork_release(f) = let
// put clean fork back on table 
// find empty place setting on table
  val (pf | ()) = mutex_lock (M_forkarr)
  val f = forkarr_addback2 (f)
  val () = condvar_signal (CV_no_fork)
  val () = mutex_unlock (pf | M_forkarr)
in
  // nothing
end // end of [fork_release]

implement
find_empty_slot (A) = 
let
  fun loop (i: int) : int = 
  if i < NPHIL then 
  let
    val n = $UN.cast{natLt(NPHIL)}(i) 
    val f = A[n]
  in
    if f != 0 then loop (i+1) else i
  end
  else ~1 // should return error or -1
in
  loop (0)
end // end of [find_empty_slot]

implement
find_non_empty_slot (A) = 
let
  fun loop (i: int) : int = 
  if i < NPHIL then 
  let
    val n = $UN.cast{natLt(NPHIL)}(i) 
    val f = A[n]
  in
    if f != 0 then i else loop (i+1)
  end
  else ~1 // should return error or -1
in
    loop (0)
end // end of [find_non_empty_slot]

(* ****** ****** *)

end // end of [local]

(* ****** ****** *)

(* end of [DiningPhil_fork.dats] *)
