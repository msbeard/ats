//
// The 6th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 17th of October, 2013
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

staload "./assignment6-2.sats"

(* ****** ****** *)

implement
mynat2int (x) =
(
case+ x of
| Z () => 0
| D0 (x) => 2 * mynat2int(x)
| D1 (x) => 2 * mynat2int(x) + 1
)

(* ****** ****** *)

implement
int2mynat (i) = (
if i > 0 then let
  val i2 = i / 2
  val res = int2mynat(i2)
in
  if i - 2*i2 = 0 then D0 (res) else D1 (res)
end else Z // end of [if]
) // end of [int2mynat]

(* ****** ****** *)

implement
fprint_mynat (out, x) = fprint! (out, "mynat(", mynat2int(x), ")")

(* ****** ****** *)

implement{
} pow2_int (n) =
(
  if n > 0 then let
    val res = pow2_int (n-1) in res + res
  end else D1(Z) // end of [if]
)

(* ****** ****** *)

implement{} 
isgtz_mynat (x) =
(
case+ x of
| Z () => false 
| D0 (x) => isgtz_mynat (x) 
| D1 _ => true
)

implement{} 
digit_sum (x) = let
//
fun loop (x: mynat, res: int): int =
  if isgtz_mynat (x) then loop (x / 10, res + x mod 10) else res
//
in
  loop (x, 0)
end // end of [digit_sum]

(* ****** ****** *)
//
// Please write your code here
//
(* ****** ****** *)

implement 
add_mynat_mynat (x, y) = 
(
case+ (x, y) of
| (_, Z () ) => x
| (Z (), _ ) => y
| (D0 (x), D0 (y)) => D0 (x + y) 		// 0+0 = 0
| (D0 (x), D1 (y)) => D1 (x + y) 		// 0+1 = 1
| (D1 (x), D0 (y)) => D1 (x + y) 		// 1+0 = 1
| (D1 (x), D1 (y)) => D0 (D1 (Z ()) + x + y)	// 1+1 = 10
)

(* ****** ****** *)

// Same length
extern
fun mynat_len (x: mynat) : int

// Pad 0's
extern 
fun append0_mynat (x: mynat, i: int) : mynat

implement
mynat_len (x) = 
(
case+ x of
| Z () => 0
| _ => 1 + mynat_len (x)
)

implement
append0_mynat (x, i) = 
let
fun loop (i: int, res: mynat) : mynat = 
if i > 0 then
 loop (i-1, D0 (res))
else res
in
loop (i, x)
end

extern
fun borrow (x: mynat) : mynat

implement 
borrow (x) = 
(
case+ x of 
| Z () => $raise NegativeMyNatExn()
| D0 (x) => D1 (borrow (x))
| D1 (x) => D0 (x) 
)

implement
sub_mynat_mynat (x, y) = 
(
case+ (x, y) of
| (_, Z () ) => x
| (Z (), _ ) => $raise NegativeMyNatExn()	// raise exception!
| (D0 (x), D0 (y)) => D0 (x - y) 		// 0-0 = 0
| (D0 (x), D1 (y)) => D1 (borrow(x) - y)	// 0-1 = 1 and borrow 1 from next more significant bit
| (D1 (x), D0 (y)) => D1 (x - y) 		// 1-0 = 1
| (D1 (x), D1 (y)) => D0 (x - y)		// 1-1 = 0
)

(* ****** ****** *)

// Recusive implementation
implement 
mul_mynat_int (x, i) = 
if i = 0 then Z () 
else if i = 1 then x
else x + x * (i - 1)

// First attempt at multiplication
(*
(
case+ x of
| Z () => Z () 
| _ => let
fun loop (i: int, res: mynat) : mynat = 
    if i > 0 then 
	loop (i-1, x + res) 
    else res
in
  loop (i, Z ())
end 
)
*)

(* ****** ****** *)
// MB: My < > <= >= eq operations are not working correctly...
// So I have to convert mynat to integer

// Recursive version
implement 
div_mynat_int (x, i) = 
if i = 0 then $raise DivideByZeroExn
// else if x < int2mynat(i) then int2mynat (0)
else if mynat2int (x) < i then int2mynat (0)
else int2mynat(1) + ((x - int2mynat (i) ) / i)

// Non-recursive version
(*
implement 
div_mynat_int (x, i) = 
(
case+ x of 
| Z () => Z ()
| _ =>
if i > 0 then
let
  val d = int2mynat (i)
  fun loop (res: mynat, j: mynat) : mynat = 
    if res >= d then
      loop (res - d, j + int2mynat (1))
    else j
in
  loop (x, Z () )
end
else $raise DivideByZeroExn 
)
*)
(* ****** ****** *)

// Recursive version
implement 
mod_mynat_int (x, i) = 
if i = 0 then $raise DivideByZeroExn
else if mynat2int(x) < i then mynat2int (x)
else (x - int2mynat (i) ) mod i

// Non-recursive version
(*
implement
mod_mynat_int (x, i) = 
(
case+ x of
| Z () => 0
| _ =>
if mynat2int(x) < i then 0
 else
let
  val d = int2mynat (i)
  fun loop (i: int, res: mynat) : mynat = 
  if res >=d then
	loop (i - 1, res - d)
  else res
in
  mynat2int(loop (i, x))
end
)
*)

(* ****** ****** *)
implement lt_mynat_mynat (x, y) = 
(
case+ (x, y) of 
| (Z (), Z () ) => false
| (_, Z () ) => false
| (Z (), _ ) => true 
| (D0 (x), D0 (y)) => false != x < y
| (D0 (x), D1 (y)) => true != x < y
| (D1 (x), D0 (y)) => false != x < y
| (D1 (x), D1 (y)) => false != x < y
)

// Need to move on
implement lte_mynat_mynat (x, y) = 
let
  val xn = mynat2int (x)
  val yn = mynat2int (y)
in
  xn <= yn
end

implement gt_mynat_mynat (x, y) = 
(
case+ (x, y) of
| (Z (), Z () ) => false
| (_, Z () ) => true
| (Z (), _ ) => false
| (D0 (x), D0 (y)) => false != x > y
| (D0 (x), D1 (y)) => false != x > y
| (D1 (x), D0 (y)) => true != x > y
| (D1 (x), D1 (y)) => false != x > y
)

// This is the equivalent of saying ~!=
extern fun xnor (x: bool, y: bool) : bool
implement xnor (x, y) = 
(
case+ (x, y) of
| (true, true) => true
| (true, false) => false
| (false, true) => false 
| (false, false) => true
)

// Need to move on
implement gte_mynat_mynat (x, y) =
let
  val xn = mynat2int (x)
  val yn = mynat2int (y)
in
  xn >= yn
end
(* ****** ****** *)

(* end of [mysolution6-2.dats] *)
