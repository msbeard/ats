//
// Student Name: Michelle Beard
// Student BUID: U17422789
//

/* ****** ****** */
/*
(*
//
// Please find a detailed description at:
//
// http://www.ats-lang.org/EXAMPLE/EFFECTIVATS/word-counting/
//
 *)
 */
/* ****** ****** */
//
// Please put your code in this file
//
/* ****** ****** */

/** Notes: Great example usage of GLIB hash table at 
 http://www.ibm.com/developerworks/linux/tutorials/l-glib/section5.html
 **/

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <glib.h>

typedef unsigned int uint32;

// Head Node of list
list0_stringANDint head;

/** Insert data **/
list0_stringANDint list0_insert(stringANDint pair) {
    list0_stringANDint ptr = (list0_stringANDint) malloc(sizeof (list0_stringANDint));

    if (ptr == NULL) {
        fprintf(stderr, "Unable to create list entry!\n");
        free(ptr);
        exit(1);
    }

    ptr->data = pair;
    ptr->next = NULL;
    
    if (head != NULL)
        ptr->next = head;
    head = ptr;
    return ptr;
}

/** Print list to stdin **/
void list0_print() {
    list0_stringANDint ptr = head;
    while (ptr != NULL) {
        printf("%s ==> %d\n", ptr->data._0_, ptr->data._1_);
        ptr = ptr->next;
    }
}

/** Quick and dirty way to append chars. Only works on Linux systems **/
string append(const string oldstring, const char c) {
    uint32 result;
    string newstring;
    result = asprintf(&newstring, "%s%c", oldstring, c);
    if (result == -1) newstring = NULL;
    return newstring;
}

/** Helper method to iterate over a GLIB Hash_table **/
void iterator(gpointer key, gpointer value, gpointer user_data) {
    printf(user_data, key, value);
}

/** Print internal Hash_table to stdin **/
void printHashMap(wcmap map) {
    g_hash_table_foreach(map, (GHFunc) iterator, "%s ==> %d\n");
}

/** Get word from stdin **/
string word_get() {
    uint32 ch, word = 0;
    string buffer = "";
    do {
        ch = getchar();
        if (ispunct(ch) || isspace(ch) || isdigit(ch)) {
            if (word) /* Have we created a word yet? */ {
                word = 0;
                return buffer;
            }
        } else {
            word = 1;
            buffer = append(buffer, tolower(ch));
        }
    } while (ch != EOF);
    return NULL;    
}

/** Create empty map **/
wcmap wcmap_create() {
    GHashTable* hash_tbl = g_hash_table_new(g_str_hash, g_str_equal);
    return hash_tbl;
}

/** Increment map by 1 **/
void wcmap_incby1(wcmap map, string str) {

    gpointer val = g_hash_table_lookup(map, str);
    if (val == NULL) {
        g_hash_table_insert(map, str, GINT_TO_POINTER(1)); 
    } else {
        gint itm = GPOINTER_TO_INT(val);
        g_hash_table_insert(map, str, GINT_TO_POINTER(itm + 1)); // increment value
    }
}

/** Helper method to insert an stringANDint item to list **/
void insert_iterator(gpointer key, gpointer value) {
    stringANDint itm = {key, GPOINTER_TO_INT(value)};
    list0_insert(itm);
}

/** Given a map, return a (unsorted) list representation **/
list0_stringANDint wcmap_listize(wcmap map) {
    g_hash_table_foreach(map, (GHFunc) insert_iterator, NULL);
    return head;
}

/* end of [mysolution1.c] */