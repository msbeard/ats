//
// The 4th assignment
// BU CAS CS520, Fall, 2013
//

(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

staload "./assignment4.sats"
dynload "./assignment4.sats"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"
staload _(*anon*) = "libats/ML/DATS/list0.dats"

(* ****** ****** *)

staload _ = "mysolution4.dats"

(* ****** ****** *)

val () =
{
//
val out = stdout_ref
//
val xs1 = (list0)$arrpsz{int}(1, 2, 3, 4)
val xs2 = (list0)$arrpsz{int}(5, 6, 7, 8)
val mat = cons0{list0(int)}(xs1, cons0{list0(int)}(xs2, nil0()))
//
val () = assertloc (matlist_get_at<int> (mat, 0, 2) = 3)
val () = assertloc (matlist_get_at<int> (mat, 1, 3) = 8)
//
val () = fprintln! (out, "mat = ")
val () = fprint_matlist<int> (out, mat)
val () = fprint_newline (out)
//
val () = assertloc (matlist_get_nrow<int> (mat) = 2)
//
val mat2 = matlist_transpose<int> (mat, 2, 4)
//
val () = fprintln! (out, "mat2 = ")
val () = fprint_matlist<int> (out, mat2)
val () = fprint_newline (out)
//
} (* end of [val] *)

(* ****** ****** *)

implement
main0 () =
{
//
val () = println! ("Your code has passed all the tests given here.")
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [test.dats] *)
