//
// The 3rd assignment
// BU CAS CS520, Fall, 2013
//

(* ****** ****** *)

(*
Due: Thursday, the 26th of September, 2013
*)

(* ****** ****** *)

#define ATS_PACKNAME "BUCASCS520.A3"

(* ****** ****** *)
//
#include "share/atspre_staload.hats"
//
(* ****** ****** *)
//
//
// HX: This function
// monitors a given directory
//
fun dirmonitor (dirname: string): void

(* ****** ****** *)

abstype dirsnap_type
typedef dirsnap = dirsnap_type
//
// HX: This function takes
// a snapshot of a given directory
//
fun dirsnap_take (dirname: string): dirsnap
//
(* ****** ****** *)

(* end of [dirmonitor.sats] *)
