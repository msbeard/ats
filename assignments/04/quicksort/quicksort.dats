//
// Animating quicksort
//

(* ****** ****** *)
#include "share/atspre_staload.hats"
staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"
staload "libats/ML/SATS/array0.sats"
staload _(*anon*) = "libats/ML/DATS/array0.dats"
staload UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

// [intqsort] is for sorting an integer array segment
extern
fun intqsort (A: array0 (int)): void

// [swap] is for swapping two integer elements in an array
extern
fun swap (A: array0 (int), i: int, j: int): void

// [partition] Partition an array [st:len] s.t. element less than the pivot are in
// in the front and elements greater than the pivot are in the rear. Return the 
// length of the front.
extern 
fun partition (A: array0 (int), st: int, len: int, pivot: int) : int

// [pivot] Determine the pivot of an array (i.e. the middle entry)
// This is actually the index of the pivot 
extern
fun pivot (A: array0 (int), st: int, len: int) : int

// [quicksort] Quicksort method
extern 
fun quicksort (A: array0 (int) , st: int, len: int) : void

(* ****** ****** *)

implement
swap (A, i, j) =
{
  val tmp = A[i]
  val () = A[i] := A[j]
  val () = A[j] := tmp
} (* end of [array0_swap] *)

implement 
pivot (A, st, len) = 
let
  val last = pred(st+len)
in
  last
end

implement 
partition (A, st, len, pivot) = 
let
  val last = pred(st+len)
  val () = println! ("Last = ", last)
  val piv_val = A[pivot] 
  val () = println! ("Pivot = ", piv_val)
  val () = println! ("Pivot index = ", pivot)
  val () = swap (A, pivot, last) // move pivot to end
  fun loop (i: int, sIndx: int) : int =
    if i < len then 
      if A[i] < piv_val then
	  (
	   //println! ("i = ", i); 
	   //println! ("sindx = ", sIndx); 
	   swap (A, i, sIndx); 
	   loop (succ(i), succ(sIndx))
	  )
      else loop (succ(i), sIndx)
    else sIndx
val sIndx = loop (st, st)
val () = println! ("SINDX = ", sIndx)
val () = swap (A, sIndx, last)
in
 (sIndx - st)
end (* end of [partition] *) 

implement 
intqsort (A) = quicksort (A, 0, $UN.cast{int}(array0_get_size (A)))

implement
quicksort (A, st, len) = 
(
if len >= 2 then let
  val pivot = pivot (A, st, len)
  val sIndx = partition (A, st, len, pivot)
  val () = quicksort (A, st, sIndx)
  val () = quicksort (A, succ(st+sIndx), pred(len-sIndx))
in
// nothing
end else ()
) (* end of [quicksort] *)
(* ****** ****** *)

implement
main0{n}
(
  argc, argv
) = let
//
val xs = let
//
fun
loop{i:nat | i <= n}
(
  argv: !argv(n), i: int (i), xs: list0(int)
) : list0(int) =
(
  if i < argc then let
    val x = g0string2int (argv[i]) in
    loop (argv, i+1, list0_cons{int}(x, xs))
  end else xs // end of [if]
)
//
in
  loop (argv, 1(*i*), list0_nil)
end // end of [val]
//
val A = array0_make_rlist (xs)
//
in
  fprint_array0_sep (stdout_ref, A, ", ");
  fprint_newline (stdout_ref);
  intqsort (A);
  fprint_array0_sep (stdout_ref, A, ", ");
  fprint_newline (stdout_ref);
end // end of [main0]

(* ****** ****** *)

(* end of [quicksort.dats] *)
