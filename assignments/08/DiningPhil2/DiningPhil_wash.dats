(* ****** ****** *)
//
// MB-2013-10-31
//
// A straightforward implementation
// of the problem of Dining Philosophers
// with a washer
//
(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

staload "./DiningPhil2.sats"

(* ****** ****** *)

extern
fun cleaner_wash2 (f: !fork): void

(* ****** ****** *)

implement
cleaner_wash () = let
//
  val () = println! ("Washer is getting ready")
  val () = randsleep (6) 
  val () = println! ("Washer is ready to clean!")
  val f = cleaner_acquire_fork ()
//
  val () = cleaner_wash2 (f)
//
  val () = cleaner_release_fork (f)
//
in
  // nothing
end // end of [cleaner_wash]

(* ****** ****** *)

implement
cleaner_wash2 (f) =
{
//
val () = println! ("Cleaner starts washing.") 
val () = randsleep (3)
val () = println! ("Cleaner finishes washing.") 
//
} (* end of [cleaner_wash2] *)

(* ****** ****** *)

(* end of [DiningPhil_wash.dats] *)
