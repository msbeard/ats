//
// The 6th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 17th of October, 2013
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

staload A6 = "./assignment6.sats"
staload A62 = "./assignment6-2.sats"
staload _(*anon*) = "./mysolution6-2.dats"

(* ****** ****** *)

dynload "./assignment6.sats"
dynload "./mysolution6.dats"

(* ****** ****** *)

dynload "./assignment6-2.sats"
dynload "./mysolution6-2.dats"

(* ****** ****** *)

implement
main0 () =
{
//
(*
val fact10 = $A6.fact_mynat ($A6.int2mynat(10))
val () = fprintln! (stdout_ref, "fact(10) = ", fact10)
*)
//

// test values for $A6
(*
val zero = $A6.int2mynat(0)
val one = $A6.int2mynat(1)
val two = $A6.int2mynat(2)
val three = $A6.int2mynat(3)
val eight = $A6.int2mynat(8)
*)

// test for $A6.log2
(*
val () = fprintln! (stdout_ref, "log(1) = 0 = ", $A6.log2_mynat(one))
val () = fprintln! (stdout_ref, "log(2) = 1 = ", $A6.log2_mynat(two))
val () = fprintln! (stdout_ref, "log(0) = 0 = ", $A6.log2_mynat(zero))
val () = fprintln! (stdout_ref, "log(8) = 3 = ", $A6.log2_mynat(eight))
*)

// test for $A6.power
(*
val () = fprintln! (stdout_ref, "1 = ", $A6.pow_mynat_mynat(two, zero))
val () = fprintln! (stdout_ref, "2 = ", $A6.pow_mynat_mynat(two, one))
val () = fprintln! (stdout_ref, "8 = ", $A6.pow_mynat_mynat(two, three))
*)

(* test values for $A62 *)

val zero = $A62.int2mynat(0)
val one = $A62.int2mynat(1)
val two = $A62.int2mynat(2)
val three = $A62.int2mynat(3)
val four = $A62.int2mynat(4)
val five = $A62.int2mynat(5)
val eight = $A62.int2mynat(8)

val () = fprintln! (stdout_ref, "8 = ", $A62.D0($A62.D0($A62.D0($A62.D1($A62.Z() ) ) ) ))
val () = fprintln! (stdout_ref, "1 = ", $A62.D1 ($A62.Z () ))
val () = fprintln! (stdout_ref, "0 = ", $A62.D0 ($A62.Z () ))
val () = fprintln! (stdout_ref, "2^3 = 8 = ", $A62.pow2_int(3))
val () = fprintln! (stdout_ref, "2^6 = 64 = ", $A62.pow2_int(6))
val () = fprintln! (stdout_ref, "2^16 = 65536 = ", $A62.pow2_int(16))
val () = fprintln! (stdout_ref, "0 + 1 = 1 = ", $A62.add_mynat_mynat(zero, one))
val () = fprintln! (stdout_ref, "3 + 8 = 11 = ", $A62.add_mynat_mynat(three, eight))
val () = fprintln! (stdout_ref, "8 + 8 = 16 = ", $A62.add_mynat_mynat(eight, eight))
val () = fprintln! (stdout_ref, "2 + 2 = 4 = ", $A62.add_mynat_mynat(two, two))
val () = fprintln! (stdout_ref, "8 + 7 = 15 = ", $A62.add_mynat_mynat($A62.int2mynat(8), $A62.int2mynat(7)))
val () = fprintln! (stdout_ref, "48 + 8 = 56 = ", $A62.add_mynat_mynat($A62.int2mynat(48), eight))
val () = fprintln! (stdout_ref, "56 + 7 = 63 = ", $A62.add_mynat_mynat($A62.int2mynat(56), $A62.int2mynat(7)))
val () = fprintln! (stdout_ref, "56 + 8 = 64 = ", $A62.add_mynat_mynat($A62.int2mynat(56), eight))
val () = fprintln! (stdout_ref, "100 + 100 = 200 = ", $A62.add_mynat_mynat($A62.int2mynat(100), $A62.int2mynat(100)))

val () = fprintln! (stdout_ref, "1 - 0 = 1 = ", $A62.sub_mynat_mynat(one, zero))
val () = fprintln! (stdout_ref, "14 - 8 = 6 = ", $A62.sub_mynat_mynat($A62.int2mynat(14), $A62.int2mynat(8)))
val () = fprintln! (stdout_ref, "8 - 8 = 0 = ", $A62.sub_mynat_mynat(eight, eight))
val () = fprintln! (stdout_ref, "9 - 4 = 5 = ", $A62.sub_mynat_mynat($A62.int2mynat(9), four))
val () = fprintln! (stdout_ref, "51 - 22 = 29 = ", $A62.sub_mynat_mynat($A62.int2mynat(51), $A62.int2mynat(22)))
// val () = fprintln! (stdout_ref, "22 - 51 = 29 = ", $A62.sub_mynat_mynat($A62.int2mynat(22), $A62.int2mynat(51)))

val () = fprintln! (stdout_ref, "1 * 0 = 0 = ", $A62.mul_mynat_int(one, 0))
val () = fprintln! (stdout_ref, "8 * 3 = 24 = ", $A62.mul_mynat_int(eight, 3))
val () = fprintln! (stdout_ref, "8 * 8 = 64 = ", $A62.mul_mynat_int(eight, 8))

val () = fprintln! (stdout_ref, "5 < 8 = true = ", $A62.lt_mynat_mynat(five, eight)) 
val () = fprintln! (stdout_ref, "8 < 5 = false = ", $A62.lt_mynat_mynat(eight, five)) 
val () = fprintln! (stdout_ref, "8 < 8 = false = ", $A62.lt_mynat_mynat(eight, eight)) 
val () = fprintln! (stdout_ref, "51 < 22 = false = ", $A62.lt_mynat_mynat($A62.int2mynat(51), $A62.int2mynat(22)))

val () = fprintln! (stdout_ref, "5 > 8 = false = ", $A62.gt_mynat_mynat(five, eight))
val () = fprintln! (stdout_ref, "8 > 5 = true = ", $A62.gt_mynat_mynat(eight, five))
val () = fprintln! (stdout_ref, "8 > 8 = false = ", $A62.gt_mynat_mynat(eight, eight))
val () = fprintln! (stdout_ref, "51 > 22 = true = ", $A62.gte_mynat_mynat($A62.int2mynat(51), $A62.int2mynat(22)))

val () = fprintln! (stdout_ref, "5 <= 8 = true = ", $A62.lte_mynat_mynat(five, eight))
val () = fprintln! (stdout_ref, "8 <= 5 = false = ", $A62.lte_mynat_mynat(eight, five))
val () = fprintln! (stdout_ref, "8 <= 8 = true = ", $A62.lte_mynat_mynat(eight, eight))
val () = fprintln! (stdout_ref, "2 <= 2 = true = ", $A62.lte_mynat_mynat(two, two))
val () = fprintln! (stdout_ref, "51 <= 22 = false = ", $A62.lte_mynat_mynat($A62.int2mynat(51), $A62.int2mynat(22)))

val () = fprintln! (stdout_ref, "5 >= 8 = false = ", $A62.gte_mynat_mynat(five, eight))
val () = fprintln! (stdout_ref, "8 >= 5 = true = ", $A62.gte_mynat_mynat(eight, five))
val () = fprintln! (stdout_ref, "8 >= 8 = true = ", $A62.gte_mynat_mynat(eight, eight))
val () = fprintln! (stdout_ref, "2 >= 2 = true = ", $A62.gte_mynat_mynat(two, two))
val () = fprintln! (stdout_ref, "51 >= 22 = true = ", $A62.gte_mynat_mynat($A62.int2mynat(51), $A62.int2mynat(22)))

val () = fprintln! (stdout_ref, "8 / 3 = 2 = ", $A62.div_mynat_int(eight, 3))
val () = fprintln! (stdout_ref, "8 / 8 = 1 = ", $A62.div_mynat_int(eight, 8))
val () = fprintln! (stdout_ref, "51 / 22 = 2 = ", $A62.div_mynat_int($A62.int2mynat(51), 22))
val () = fprintln! (stdout_ref, "49 / 7 = 7 = ", $A62.div_mynat_int($A62.int2mynat(49), 7))
val () = fprintln! (stdout_ref, "7 / 49 = 0 = ", $A62.div_mynat_int($A62.int2mynat(7), 49))

val () = fprintln! (stdout_ref, "8 mod 3 = 2 = ", $A62.mod_mynat_int(eight, 3))
val () = fprintln! (stdout_ref, "8 mod 8 = 0 = ", $A62.mod_mynat_int(eight, 8))
val () = fprintln! (stdout_ref, "51 mod 22 = 7 = ", $A62.mod_mynat_int($A62.int2mynat(51), 22))
val () = fprintln! (stdout_ref, "49 mod 7 = 0 = ", $A62.mod_mynat_int($A62.int2mynat(49), 7))
val () = fprintln! (stdout_ref, "7 mod 49 = 7 = ", $A62.mod_mynat_int($A62.int2mynat(7), 49))

(* 115 *)
val I = 10
val digit_sum_of_pow2_100 = $A62.digit_sum ($A62.pow2_int (I))
val () = fprintln! (stdout_ref, "digit_sum_of_pow2_100 ( ", $A62.pow2_int(I), " ) = ", digit_sum_of_pow2_100)

// val () = fprintln! (stdout_ref, "1000 = ", $A6.int2mynat(1000))
// val () = fprintln! (stdout_ref, "1000 = ", $A62.int2mynat(1000))

} (* end of [main0] *)

(* ****** ****** *)

(* end of [test.dats] *)
