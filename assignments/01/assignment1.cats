/*
extern fun word_get (): string = "mac#"
extern fun wcmap_create (): wcmap = "mac#"
extern fun wcmap_incby1 (map: wcmap, w: string): void = "mac#"
extern fun wcmap_listize (map: wcmap): list0 @(string, int)
*/

typedef void* wcmap ;
typedef char* string ;

typedef
    struct { string _0_; int _1_ ; }
stringANDint ;

typedef
struct _list0_stringANDint
{
stringANDint data ;
struct _list0_stringANDint *next ;
} *list0_stringANDint ;

extern char *word_get () ;
extern wcmap wcmap_create () ;
extern void wcmap_incby1 (wcmap, string) ;
extern list0_stringANDint wcmap_listize (wcmap) ;

/* ****** ****** */
//
#include "mysolution1.c" // your code stays in "mysolution1.c"
//
/* ****** ****** */

/* end of [assignment1.cats] */
