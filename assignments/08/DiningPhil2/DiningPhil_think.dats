(* ****** ****** *)
//
// HX-2013-10-31
//
// A straightforward implementation
// of the problem of Dining Philosophers
// with a washer
//
(* ****** ****** *)

#define ATS_DYNLOADFLAG 0

(* ****** ****** *)

staload "./DiningPhil2.sats"

(* ****** ****** *)

implement
phil_think (n) =
{
//
val () = println! ("Phil(", n, ") starts thinking") 
val () = randsleep (6)
val () = println! ("Phil(", n, ") finishes thinking") 
//
} (* end of [phil_think] *)

(* ****** ****** *)

(* end of [DiningPhil_think.dats] *)
