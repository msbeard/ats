All class assignemnts are stored here. To learn more about ATS, go to http://www.ats-lang.org/

===

ATS is a statically typed programming language that unifies implementation with formal specification. 
It is equipped with a highly expressive type system rooted in the framework Applied Type System, which gives 
the language its name. In particular, both dependent types and linear types are available in ATS.

