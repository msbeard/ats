//
// The 4th assignment
// BU CAS CS520, Fall, 2013
//

(* ****** ****** *)

#define ATS_PACKNAME "BUCASCS520.A4"

(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"

(* ****** ****** *)

typedef
matlist (a:t@ype) = list0 (list0 (a))

(* ****** ****** *)

exception MatrixSubscriptExn of ()

(* ****** ****** *)
//
// HX: it returns M[i,j]
//
fun{a:t@ype} // 5 points
matlist_get_at (M: matlist (a), i: int, j: int): a

fun{a:t@ype}
matlist_get_row (M: matlist (a), i: int): list0(a)

fun{a:t@ype} 
matlist_get_nrow (M: matlist(a)) : int

(* ****** ****** *)
//
// This function prints out a given matrix
//
fun{a:t@ype} // 15 points
fprint_matlist (out: FILEref, M: matlist(a)): void

(* ****** ****** *)
//
// The function returns the transpose of a given matrix
//
fun{a:t@ype}
matlist_transpose // 20 points
  (M: matlist (a), nrow: int, ncol: int): matlist (a)
// end of [matlist_transpose]

(* ****** ****** *)

(* end of [assignment4.sats] *)
