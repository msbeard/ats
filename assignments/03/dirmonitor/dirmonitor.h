// dirmonitor.h
// Interface to monitor a directory by
// taking a snapshot of a directories contents

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <unistd.h>

typedef char * string;

// A snapshot consists of the name of the file and its status
typedef struct
{
        string name;
        struct stat *ls;
} *filesnap;

// A directory snapshot captures the information of an entire
// directories contents and their statistics
typedef void * dirsnap;

// Dirmonitor
extern void dirmonitor (string dirname);

// Get all sys information of a file
extern void getFileInfo (string filename, filesnap fs);

// Print contents of a filesnapshot to stdout
extern void printFileContents (filesnap fs);

// Print contents of a directory napshot to stdout
extern void printContents (dirsnap ds);

// Allocate space needed to create a filesnapshot
extern filesnap createFilesnap ();

// Allocate space needed to create a snapshot
extern dirsnap createDirsnap ();

// Compare two filesnapshots
extern int cmpFilesnap (filesnap prev, filesnap curr);

// Compare two directory snapshots 
extern int cmpDirsnap (dirsnap prev,  dirsnap curr);
