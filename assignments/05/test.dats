(*
//
// The 5th assignment
// BU CAS CS520, Fall, 2013
//
*)

(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

staload "./assignment5.sats"
dynload "./assignment5.sats"
dynload "./mysolution5.dats"

(* ****** ****** *)

fun
pi_stream_gen (
) : stream (double) = let
//
// pi/4 = 1 - 1/3 + 1/5 - 1/7 + ...
//
fun aux (
 n: int, sgn: int, sum0: double
) : stream (double) = $delay
(
let
  val sum1 = sum0 + 4.0*sgn/n
in
  stream_cons{double}(sum1, aux (n+2, ~sgn, sum1))
end
)
//
in
  aux (1, 1, 0.0)
end // end of [pi_stream_gen]

(* ****** ****** *)

fun stream_dups
(
  xs0: stream(int)
) : stream(int) = let
  val-stream_cons(x0, xs1) = !xs0
  val-stream_cons(x1, xs2) = !xs1
in
  if x0 = x1 then
    $delay (stream_cons{int}(x0, stream_dups (xs1))) else stream_dups (xs1)
end // end of [stream_dups]

(* ****** ****** *)

fun
the_Ramanujans_gen (): stream(int) = let
  fn f (x:int, y:int):<> int = x*x*x + y*y*y
  val xys = intpair_enumerate_with_ordering (f)
  val sum3s = stream_map_fun<int2><int> (xys, lam xy => f (xy.0, xy.1))
in
  stream_dups (sum3s)
end // end of [the_Ramanujans_gen]

(* ****** ****** *)

implement
main0 () =
{
//
val pi_stream = pi_stream_gen ()
val pi_stream = EulerTrans (pi_stream)
val pi_stream = EulerTrans (pi_stream)
val pi_stream = EulerTrans (pi_stream)
val pi_stream = EulerTrans (pi_stream)
val pi_stream = EulerTrans (pi_stream)
val pi_stream = EulerTrans (pi_stream)
val pi_stream = EulerTrans (pi_stream)
val-stream_cons (pi, _) = !pi_stream
val () = print! ("pi = ")
val () = $extfcall (void, "printf", "%.18f", pi)
val () = print_newline ()
//
val ijs = intpair_enumerate ()
//
val () = println! ("intpair_enumerate:")
//
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
val-stream_cons(ij, ijs) = !ijs
val () = println! ("(", ij.0, ", ", ij.1, ")")
//
val ijs =
intpair_enumerate_with_ordering (lam (x, y) => x + y)
//
val () = println! ("intpair_enumerate_with_ordering:")
//
val ijs20 = stream_take_exn (ijs, 20)
//
implement
fprint_val<int2> (out, x) =
  fprint! (out, "(", x.0, ", ", x.1, ")")
implement
fprint_list_vt$sep<> (out) = fprint (out, "\n")
//
val () = fprintln! (stdout_ref, "ijs(20) =\n", ijs20)
//
val () = list_vt_free (ijs20)
//

val-stream_cons (n, _) = !(the_Ramanujans_gen())
val () = println! ("The first Ramanujan number is ", n)
//

val x = @(0, 0) 
val y = @(1, 2)
val () = println! ("Is monotone = ", ismonotone (lam (x, y) => x + y, x, x))
val () = println! ("Is monotone = ", ismonotone (lam (x, y) => x + y, x, y))
val () = println! ("Is monotone = ", ismonotone (lam (x, y) => x + y, y, x))

} (* end of [main0] *)

(* ****** ****** *)

(* end of [test.dats] *)
