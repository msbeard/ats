(*
**
** A gtkcairoclock template
**
**
*)

(* ****** ****** *)

(*
Assignment 3:
Class: BU CAS CS520, Fall, 2013
Due: Thursday, the 26th of September, 2013
*)

(* ****** ****** *)
//
#include "share/atspre_define.hats"
#include "share/atspre_staload.hats"
//
(* ****** ****** *)

staload
UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

// Time functions
staload "libc/SATS/time.sats"

(* ****** ****** *)

staload "{$CAIRO}/SATS/cairo.sats"

(* ****** ****** *)

%{^
typedef
struct { char buf[32] ; } bytes32 ;
%} // end of [%{^]
abst@ype bytes32 = $extype"bytes32"

(* ****** ****** *)

%{^
#define mystrftime(bufp, m, fmt, ptm) strftime((char*)bufp, m, fmt, ptm)
%} // end of [%{^]

(* ****** ****** *)

extern fun mydraw_clock (cr: !cairo_ref1, width: int, height: int): void = "ext#mydraw_clock"
// end of [mydraw_clock]

(* ****** ****** *)

// This function renders the date and time to a window
extern fun draw_time (cr : !cairo_ref1, wd : int, ht : int) : void

// This function renders a transparent rectangle bordering the time string
extern fun draw_time_box (cr : !cairo_ref1, wd : double, ht : double) : void

// This function renders a string in a given position
extern fun draw_string
(cr : !cairo_ref1, wd : int, ht : int, str : string, sz : double) : void

// This function simply set the background to be blue
extern fun set_background
(cr : !cairo_ref1, wd : double, ht : double) : void

// This function renders a yellow star in a given position
extern fun draw_star
(cr : !cairo_ref1, wd : int, ht : int) : void

// Set background color to blue
implement set_background (cr, wd, ht) = () where
{
  val () = cairo_rectangle (cr, 0.0, 0.0, wd, ht)
  val () = cairo_set_source_rgb (cr, 0.0, 0.0, 0.5)
  val () = cairo_fill (cr)
}

// Draw yellow-green star
implement draw_star (cr, wd, ht) = () where
{
  val () = cairo_set_source_rgb (cr, 0.0, 0.7, 0.0)
  val () = cairo_line_to (cr, 0.0, 85.0)
  val () = cairo_line_to (cr, 75.0, 75.0)
  val () = cairo_line_to (cr, 100.0, 10.0)
  val () = cairo_line_to (cr, 125.0, 75.0)
  val () = cairo_line_to (cr, 200.0, 85.0)
  val () = cairo_line_to (cr, 150.0, 125.0)
  val () = cairo_line_to (cr, 160.0, 190.0)
  val () = cairo_line_to (cr, 100.0, 150.0)
  val () = cairo_line_to (cr, 40.0, 190.0)
  val () = cairo_line_to (cr, 50.0, 125.0)
  val () = cairo_line_to (cr, 0.0, 85.0)
  val () = cairo_stroke_preserve (cr)
  val () = cairo_fill (cr)
}

// Draw time
implement draw_time (cr, wd, ht) = () where
{
  var time: time_t
  val yn = time_getset (time)
  val () = assert_errmsg (yn, $mylocation)
  prval () = opt_unsome{time_t}(time)
  var tm: tm_struct
  val ptr_ = localtime_r (time, tm)
  val () = assert_errmsg (ptr_ > 0, $mylocation)
  prval () = opt_unsome {tm_struct} (tm)
  var buf: bytes32?
  val bufp = addr@(buf)
  val _ = $extfcall (size_t, "mystrftime", bufp, 32, "%m/%d/%Y %T", addr@(tm))
  val () = draw_string (cr, wd, ht, $UN.castvwtp1{string}(bufp), 26.0)
} (* end of [draw_time] *)

implement draw_string (cr, wd, ht, str, sz) = () where
{
  val () = cairo_select_font_face (cr, "Courier", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD)
  val () = cairo_set_font_size (cr, sz)
  var extents: cairo_text_extents_t
  val () = cairo_text_extents (cr, str, extents)
  val xc = (wd - extents.width) / 2
  and yc = (ht - extents.height) / 2
  val () = cairo_move_to (cr, xc - extents.x_bearing, yc - extents.y_bearing)
  val () = cairo_set_source_rgb (cr, 0.0, 0.0, 0.8)
  val () = cairo_show_text (cr, str)
}

implement draw_time_box (cr, wd, ht) = () where
{
  val () = cairo_set_source_rgba (cr, 0.0, 0.0, 1.0, 0.11) // baby blue hue
  val () = cairo_rectangle (cr, wd/10, (ht*7)/16, wd - (2*(wd/10)), ht/8) // x, y, width, height
  val () = cairo_stroke_preserve (cr)
  val () = cairo_fill (cr)
}

(* ********** *) 
implement mydraw_clock (cr, wd, ht) = () where
{
  // Need wd, ht to be doubles for some methods
  val wdd = g0int2float_int_double (wd)
  val htd = g0int2float_int_double (ht)
  val () = set_background(cr, wdd, htd)
  val () = draw_time (cr, wd, ht)
  val () = draw_time_box (cr, wdd, htd) 
  val () = draw_star (cr, wd, ht)
  val () = draw_string (cr, (wd - wd/4), (ht - ht/4), "Alarm", 30.0)
}


(* ****** ****** *)

%{^
typedef char **charptrptr ;
%} ;
abstype charptrptr = $extype"charptrptr"

(* ****** ****** *)

staload "/cs/coursedata/cs520/Fall13/myGTK/SATS/gtkcairoclock.sats"
staload _ = "/cs/coursedata/cs520/Fall13/myGTK/DATS/gtkcairoclock.dats"

(* ****** ****** *)

implement
main0 (argc, argv) =
{
//
var argc: int = argc
var argv: charptrptr = $UN.castvwtp1{charptrptr}(argv)
//
val () = $extfcall (void, "gtk_init", addr@(argc), addr@(argv))
//
implement
gtkcairoclock_title<> () = stropt_some"gtkcairoclock"
implement
gtkcairoclock_timeout_interval<> () = 500U // millisecs
implement
gtkcairoclock_mydraw<> (cr, width, height) = mydraw_clock (cr, width, height)
//
val ((*void*)) = gtkcairoclock_main ((*void*))
//
} (* end of [main0] *)

(* ****** ****** *)

(* end of [myclock.dats] *)
