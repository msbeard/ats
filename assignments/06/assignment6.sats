//
// The 6th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 17th of October, 2013
//
(* ****** ****** *)

datatype
mynat = Z of () | S of mynat

(* ****** ****** *)
//
// HX: these are implemented
//
fun mynat2int (mynat): int
fun int2mynat (i: int): mynat
//
fun fprint_mynat (out: FILEref, x: mynat): void
overload fprint with fprint_mynat
//
(* ****** ****** *)
//
// HX: these are implemented
//
fun add_mynat_mynat : (mynat, mynat) -> mynat
fun sub_mynat_mynat : (mynat, mynat) -> mynat
fun mul_mynat_mynat : (mynat, mynat) -> mynat
fun div_mynat_mynat : (mynat, mynat) -> mynat
fun mod_mynat_mynat : (mynat, mynat) -> mynat
//
overload + with add_mynat_mynat
overload - with sub_mynat_mynat
overload * with mul_mynat_mynat
overload / with div_mynat_mynat
overload mod with mod_mynat_mynat
//
(* ****** ****** *)
//
// HX: these are implemented
//
fun lt_mynat_mynat : (mynat, mynat) -> bool
fun lte_mynat_mynat : (mynat, mynat) -> bool
fun gt_mynat_mynat : (mynat, mynat) -> bool
fun gte_mynat_mynat : (mynat, mynat) -> bool
fun eq_mynat_mynat : (mynat, mynat) -> bool
fun neq_mynat_mynat : (mynat, mynat) -> bool
//
overload < with lt_mynat_mynat
overload <= with lte_mynat_mynat
overload > with gt_mynat_mynat
overload >= with gte_mynat_mynat
overload = with eq_mynat_mynat
overload != with neq_mynat_mynat
//
(* ****** ****** *)

fun fact_mynat (x: mynat): mynat // implemented

(* ****** ****** *)
//
// HX: 5 points
// log2(x) equals
// the largest n such that 2^n <= x
//
fun log2_mynat (x: mynat): mynat
//
(* ****** ****** *)
//
// HX: 5 points
// pow(b, e) equals b^e
//
fun pow_mynat_mynat (base: mynat, exponent: mynat): mynat
//
(* ****** ****** *)

(* end of [assignment6.sats] *)
