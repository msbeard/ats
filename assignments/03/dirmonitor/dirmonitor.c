// dirmonitor.c
// This application iterates over a directories
// contents and prints information about it

#include "dirmonitor.h"

#define DEBUG 0
int TIME_TO_WAIT = 10;

void getFileInfo (string filename, filesnap fs)
{
	// Check ds
	if (fs != NULL)
	{
		struct stat fst;
		if (stat(filename, &fst) != 0) 
		{
        		perror ("Stat!");
        		exit (EXIT_FAILURE);
    		}
		
		fs->name = filename;
		fs->ls = &fst;
	}
	else 
	{
		perror ("DS!");
		free (fs); 
		exit (EXIT_FAILURE);
	}
}

void printFileContents (filesnap fs)
{
	struct stat fst = *(fs->ls);
	printf("\nInformation for %s\n", fs->name);
	printf("----------------------------\n\n");
	printf("Ownership:\t UID=%ld   GID=%ld\n", (long) fst.st_uid, (long) fst.st_gid);
	printf("File size:\t %lld bytes\n", (long long) fst.st_size);
	printf("Last accessed:\t %s", ctime (&fst.st_atime));
        printf("Last modified:\t %s", ctime (&fst.st_mtime));
        printf("Last changed:\t %s", ctime (&fst.st_ctime)); 
		
}

filesnap createFilesnap ()
{
	filesnap fs;
	fs = (filesnap) malloc(sizeof (filesnap));
	return fs;
}

// dirsnap createDirsnap ()
//{
//}

void dirmonitor (string dirname)
{
	DIR *dirp;
	struct dirent *dp;
	
	if (( dirp = opendir (dirname)) == NULL)
	{
		perror ("Couldn't open directory!");
		exit (EXIT_FAILURE);
	}
	
	// Create directory and fill its contents
	// dirsnap ds = createDirsnap ();
	
	while (dp = readdir (dirp) )
	{
		filesnap fs = createFilesnap ();
		getFileInfo (dp->d_name, fs);
		// insertToDir (dp->d_name, fs, ds);
		if (DEBUG)
			printFileContents (fs);
	}
	closedir (dirp);
}

int main (int argc, char **argv)
{
	if (argc < 2)
	{
		printf ("Usage: %s directory\n", argv[0]);
		exit (EXIT_FAILURE);
	}

	string dirname = argv[1];

	printf ("Taking a snapshot of directory: %s\n", dirname);
	
	dirmonitor (dirname);

	clock_t start, curr;
	start = clock ();

	while (1)
	{
		curr = clock ();
		if (curr - start >= TIME_TO_WAIT * CLOCKS_PER_SEC)
		{
			printf ("Taking another snapshot of directory: %s\n", dirname);
			start = curr;
		}
	}

	return EXIT_SUCCESS;
}

