(*
//
// The 5th assignment
// BU CAS CS520, Fall, 2013
//
*)

(* ****** ****** *)

#include
"share/atspre_staload.hats"

(* ****** ****** *)

staload "./assignment5.sats"

(* ****** ****** *)

fun{a:t@ype}
stream_alternate
(
  xs: stream a, ys: stream a
) : stream (a) =
  $delay (stream_alternate_con<a> (xs, ys))

and
stream_alternate_con
(
  xs: stream a, ys: stream a
) : stream_con (a) = let
  val-stream_cons (x, xs) = !xs
in
  stream_cons{a}(x, stream_alternate<a> (ys, xs))
end // end of [stream_alternate_con]

(* ****** ****** *)

implement
intpair_enumerate () = let
//
fun rowgen
(
  i: int, j: int
) : stream(int2) =
  $delay(stream_cons{int2}((i, j), rowgen(i, j+1)))
//
fun nrowgen (i: int): stream(int2) =
  $delay(stream_cons{int2}((i, i), stream_alternate<int2> (rowgen(i, i+1), nrowgen(i+1))))
//
in
  nrowgen (0)
end // end of [intpair_enumerate]

(* ****** ****** *)
//
// Please put your code here
//

(* ****** ****** *)
// EulerTrans
implement EulerTrans (xs: stream (double) ) : stream ( double ) = 
let
  // What is needed? 
  // x(n) => current value => curr
  // x(n+1) => next value => next
  // x(n+2) => value after next => succ
  val-stream_cons (curr, xs1) = !xs
  val-stream_cons (next, xs2) = !xs1
  val-stream_cons (succ, _) = !xs2

  // Compute some partial sums and agg
  // d0 = (x(n+2)-x(n+1))
  // d1 = 2*x(n+1)
  val d0 = succ - next 
  val d1 = 2 * next
  val dividend = (d0*d0) 
  val divisor = curr + succ - d1
  val agg = succ - (dividend / divisor)
in
  $delay (stream_cons{double} (agg, EulerTrans (xs1)))
end (* end of [EulerTrans] *)

(* ****** ****** *)

(* ****** ****** *)
// helper method
implement ismonotone (f: (int, int) -<fun0> int, x: int2, y: int2) : bool = 
let
  val ans0 = f (x.0, x.1)
  val ans1 = f (y.0, y.1)
  val sgnf = gcompare_val<int> (ans0, ans1)
  val sgnx = gcompare_val<int> (x.0, y.0)
  val sgny = gcompare_val<int> (x.1, y.1)
in
  if (sgnf <= 0 && sgnx <= 0 && sgny <= 0) then
    true
  else false
end

fun recurfun (f: (int, int) -<fun0> int, ijs: stream (int2)) : stream (int2) = 
let
  val-stream_cons (first, ijs0) = !ijs
  val-stream_cons (next, ijs1) = !ijs0
in
  (*
  if ismonotone (f, first, next) then
    $delay (stream_cons{int2} (first, recurfun (f, ijs0)))
  else $delay (stream_cons{int2} (next, recurfun (f, ijs1)))
  *)
  if f(first.0, first.1) < f(next.0, next.1) then
    $delay (stream_cons{int2} (first, recurfun (f, ijs0)))
  else if f(first.0, first.1) = f(next.0, next.1) && (first.0 < next.0) && (first.1 < next.1) then
    $delay (stream_cons{int2} (first, recurfun (f, ijs0)))
  else $delay (stream_cons{int2} (next, recurfun (f, ijs0)))
end

// intpair_enumerate_with_ordering
implement intpair_enumerate_with_ordering (f: (int, int) -<fun0> int) : stream (int2) =
let
  val ijs = intpair_enumerate ()
//
in
  recurfun (f, ijs)
end (* end of [intpair_enumerate_with_ordering] *) 
(* ****** ****** *)

(* end of [mysolution5.dats] *)
