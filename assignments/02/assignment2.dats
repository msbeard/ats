// Michelle Beard
// Tail-Recusrion 
// 2nd assignment of CS520
//

(* ****** ****** *)

#define ATS_PACKNAME "BUCASCS520.A2"

(* ****** ****** *)
//
#include "share/atspre_staload.hats"

//
(* ****** ****** *)

datatype
ilist =
  | ilist_nil of () 
  | ilist of (int, ilist)
// end of [ilist]

(* ****** ****** *)

(* ****** ****** *)
//
// E: empty tree
// B: non-empty tree
//
datatype itree = 
| E of () 
| B of (itree, int, itree)

(* ****** ****** *)
//
// The function returns the tally
// of all the integers in a given tree
//
extern // 5 pts
fun itree_tally (tr: itree): int

implement
itree_tally (tr : itree) : int = 
(
case+ tr of
| E () => 0
| B (e1, x, e2) => itree_tally(e1) + x + itree_tally(e2)
) (* end of [itree_tally] *)

(* ****** ****** *)
//
// This function returns the number of nodes
// in a given tree
//
extern // 5 pts
fun itree_size (tr: itree): int

implement 
itree_size (tr : itree): int = 
(
case+ tr of
| E() => 0
| B (e1, _, e2) => itree_size(e1) + 1 + itree_size(e2)
) (* end of [itree_size] *)

(* ****** ****** *)
//
// This function returns the number of nodes
// in a longest path from the root to a leaf
//
extern // 5 pts
fun itree_height (tr: itree): int

// This function returns the largest number
// Helper method for itree_height
extern 
fun imax (x: int, y: int) : int

implement
imax (x: int, y: int) : int = 
  if ( x >= y ) then x else y

implement 
itree_height (tr: itree): int = 
(
case+ tr of 
| E () => ~1
| B (e1, _, e2) => 1 + imax(itree_height (e1), itree_height (e2))
) (* end of [itree_height] *)

(* ****** ****** *)
//
// A tree is perfect if it is perfectly balanced
//
extern // 10 pts
fun itree_is_perfect (tr: itree): bool

// Helper method to determine the absolute value of a number
extern 
fun myabs (x: int): int

implement myabs (x: int) : int = 
  if x >= 0 then x else ~x

implement itree_is_perfect (tr: itree): bool = 
case+ tr of
| E () => true
| B (e1, _, e2) => 
let
   val HL = itree_height (e1);
   val HR = itree_height (e2);
in
   myabs (HL - HR) <= 1 && 
   itree_is_perfect(e1) &&
   itree_is_perfect(e2)
end

// Taken from test.dats online
extern
fun ilist_append (xs: ilist, ys: ilist): ilist
overload + with ilist_append

implement ilist_append (xs, ys) =
(
case+ xs of
| ilist_nil () => ys
| ilist (x, xs) => ilist (x, xs + ys)
)

(* ****** ****** *)
//
// Given a tree, the returned list is the sequence of
// nodes encountered during an inord-traversal of the tree
//
extern // 10 pts
fun itree_listize_inord (tr: itree): ilist

implement itree_listize_inord (tr: itree) : ilist = 
case+ tr of 
| E () => ilist_nil ()
| B (e1, i, e2) => ilist_append(itree_listize_inord(e1), 
			       ilist(i, itree_listize_inord(e2)))

// Given a tree, the returned list is the sequence of
// nodes encountered during an preord-traversal of the tree
extern // 10 pts
fun itree_listize_preord (tr: itree): ilist

implement itree_listize_preord(tr: itree) : ilist =
(
case+ tr of
| E() => ilist_nil ()
| B(e1, i, e2) =>
	ilist (i, ilist_append (itree_listize_preord(e1), itree_listize_preord(e2)))
)
(* end of [itree-listize_preord] *)

//
// Given a tree, the returned list is the sequence of
// nodes encountered during an postord-traversal of the tree
//
extern // 10 pts
fun itree_listize_postord (tr: itree): ilist

implement itree_listize_postord (tr: itree) : ilist =
case+ tr of 
| E () => ilist_nil ()
| B (e1, i, e2) =>
	ilist_append(
		ilist_append(itree_listize_postord(e1), itree_listize_postord(e2)), 
		ilist (i, ilist_nil ()))

(* ****** ****** *)
//
// A string in ATS is the same as a string (char*) in C:
// it is a pointer to an array of chars ending with '\0'
//

// My definition of what a string is
datatype string = 
| string_nil of ()
| string of (char, string)

// Code modified from Introduction to Programming in ATS
extern fun string_reverse_append (xs: string, ys: string): string 
extern fun string_reverse (str: string): string

implement 
string_reverse_append (xs: string, ys: string) : string = 
(
case+ xs of
 | string (c, xs) => string_reverse_append (xs, string (c, ys))
 | string_nil () => ys
) (* end of string_reverse_append] *)

implement
string_reverse (str: string) : string = 
  string_reverse_append (str, string_nil() )

// Code modified from Piazza solution
extern fun print_string (str: string): void
extern fun print_list (xs: ilist) : void

// Helper method to print a string
implement 
print_string(str: string): void = 
(
case+ str of
| string_nil() => ()
| string (c, str) => ( println!(c); print_string(str); )
)

// Helper method to print a list
implement 
print_list (xs: ilist) : void = 
case+ xs of
| ilist_nil() => ()
| ilist (x, xs) => ( print_int(x); print_newline(); print_list(xs);)

// This function tests whether to strings are equal
extern fun string_cmp (xs: string, ys: string): bool

implement string_cmp (xs: string, ys:string) : bool = 
(
  case+ xs of 
  | string (x, xs1) =>
    (
      case+ ys of 
      | string (y, ys1) => 
 	if (compare(x, y) !=0) then false
	  else string_cmp(xs1, ys1) 
      | string_nil () => true
    )
  | string_nil () => true
) 

(* ****** ****** *)
//
// This function tests whether a given string is
// a palindrome, that is, whether the string equals its reverse
//
extern // 10 pts
fun string_is_palindrome (str: string): bool

implement string_is_palindrome (str: string): bool = 
  string_cmp(str, string_reverse(str))
(* end of [string_is_palindrome] *)

// To solve the permute_test, I implemented selection sort and 
// provided two smaller methods, smallest and remove, to assist.
extern fun selection_sort (xs: string): string
extern fun smallest (xs: string, c: char): char
extern fun remove (xs: string, c: char) : string

// Look for smallest element in string
implement smallest (xs: string, c: char): char = 
case+ xs of 
| string_nil () => c
| string (c1, xs1) => 
	if c1 >= c 
	  then smallest (xs1, c)
	else 
	  smallest (xs1, c1)  
(* end of [smallest] *)

// Remove a single character from string
implement remove (xs: string, c: char) : string = 
case+ xs of 
| string_nil () => xs
| string (curr, xs1) => 
	if compare (c, curr) = 0
	  then xs1
	else string (curr, remove (xs1, c)) 
   
// Selection Sort
implement selection_sort (xs: string) : string = 
(
case+ xs of
| string_nil () => xs
| string (c, xs1) =>
  	let
	  val small = smallest (xs, c);
	  val xs1 = remove (xs, small);
	in 
	  string (small, selection_sort (xs1));
	end
)

(* ****** ****** *)
//
// This function tests
// whether one given string is a permuation of another
//
extern // 15 pts
fun string_permute_test (str1: string, str2: string): bool

implement string_permute_test (str1: string, str2: string): bool = 
let
  val str1_sort = selection_sort(str1);
  val str2_sort = selection_sort(str2);
in
  string_cmp (str1_sort, str2_sort);
end (* end of [string_premute_test] *)

(* ****** ****** *)

#define :: string

val s0 = string_nil ()
val s1 = 'm' :: 'o' :: 'm' :: s0
val s2 = 'g' :: 'o' :: 'd' :: s0
val s3 = 'a' :: 'b' :: 'a' :: 'b' :: s0
val s4 = 'a' :: 'a' :: 'b' :: 'b' :: s0
val s5 = 'a' :: 'a' :: 'b' :: 'c' :: s0
val s6 = 'd' :: 'o' :: 'g' :: s0

// Test values
val tr0 = E ()            	 (* empty tree *)
val tr1 = B (E, 1, E)    	 (* tree *)
val tr2 = B (E, 2, E)     	 (* tree *)
val unb = B (E, 2, B(tr2, 4, E)) (* unbalanced tree *)
val bal = B (tr1, 3, tr2)        (* balanced tree *)
val big = B (unb, 6, bal)        (* big tree *)

// Test itree_tally
val () = assertloc (itree_tally (tr0) = 0)
val () = assertloc (itree_tally (tr1) = 1)
val () = assertloc (itree_tally (tr2) = 2)
val () = assertloc (itree_tally (unb) = 8)
val () = assertloc (itree_tally (bal) = 6)
val () = assertloc (itree_tally (big) = 20)

// Test itree_size
val () = assertloc (itree_size (tr0) = 0)
val () = assertloc (itree_size (tr1) = 1)
val () = assertloc (itree_size (tr2) = 1)
val () = assertloc (itree_size (unb) = 3)
val () = assertloc (itree_size (bal) = 3)
val () = assertloc (itree_size (big) = 7)

// Test imax
val () = assertloc (imax(4, 5) = 5)
val () = assertloc (imax(4, 4) = 4)
val () = assertloc (imax(6, 5) = 6)

// Test itree_height
val () = assertloc (itree_height (tr0) = ~1)
val () = assertloc (itree_height (tr1) = 0)
val () = assertloc (itree_height (tr2) = 0)
val () = assertloc (itree_height (unb) = 2)
val () = assertloc (itree_height (bal) = 1)
val () = assertloc (itree_height (big) = 3)

// Tesing Balanced Tree
val () = assertloc (itree_is_perfect (tr0) = true)
val () = assertloc (itree_is_perfect (tr1) = true)
val () = assertloc (itree_is_perfect (unb) = false)
val () = assertloc (itree_is_perfect (bal) = true)
val () = assertloc (itree_is_perfect (big) = false)

// Testing Preorder traversal
#define :: ilist

// Taken from test.dats online
// Overload ilist_eq operator
extern
fun ilist_eq (xs: ilist, ys: ilist): bool
overload = with ilist_eq

implement
ilist_eq (xs, ys) =
(
case+ (xs, ys) of
| (x :: xs,
   y :: ys) => if x = y then xs = ys else false
| (ilist_nil (), ilist_nil ()) => true
| (_, _) => false
) (* end of [ilist_eq] *)

// Testing preorder, inorder, and postorder traversal
val inord = 2 :: 2 :: 4 :: ilist_nil ();
val preord = 2 :: 4 :: 2 :: ilist_nil ();
val postord = 2 :: 4 :: 2 :: ilist_nil ();

val () = assertloc (itree_listize_preord (tr0) = ilist_nil())
val () = assertloc (itree_listize_preord (unb) = preord)
val () = assertloc (itree_listize_inord (unb) = inord)
val () = assertloc (itree_listize_postord (unb) = postord)

// Testing string reverse
val () = assertloc (string_cmp (s2, string_reverse(s2)) = false)
val () = assertloc (string_cmp (s2, s2) = true)

// Testing smallest
val () = assertloc (smallest (s1, 'k') = 'k')
val () = assertloc (smallest (s2, 'd') = 'd')
val () = assertloc (smallest (s1, 'm') = 'm')

// Testing string palindrome
val () = assertloc (string_is_palindrome(s1) = true)
val () = assertloc (string_is_palindrome(s2) = false)

// Tesing permute test
val () = assertloc (string_permute_test (s2, s6) = true)
val () = assertloc (string_permute_test (s3, s4) = true)
val () = assertloc (string_permute_test (s4, s5) = false)
val () = assertloc (string_permute_test (s1, s2) = false)

implement 
main0 () = () where
{
  val () = println! ("Just using assert statements!")
}

(* end of [assignment2.dats] *)
