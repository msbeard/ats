//
// The 6th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 17th of October, 2013
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

staload "./assignment6.sats"

(* ****** ****** *)

implement
mynat2int (x) = let
//
fun loop (x: mynat, res: int): int =
  case+ x of Z() => res | S(x) => loop (x, res+1)
//
in
  loop (x, 0)
end // end of [mynat2int]

(* ****** ****** *)

implement
int2mynat (i) = let
//
fun loop (i: int, res: mynat): mynat =
  if i > 0 then loop (i-1, S(res)) else res
//
in
  loop (i, Z)
end // end of [int2mynat]

(* ****** ****** *)

implement
fprint_mynat (out, x) = fprint! (out, "mynat(", mynat2int(x), ")")

(* ****** ****** *)

implement
add_mynat_mynat (x, y) =
(
case+ x of
| Z () => y 
| S (x) => x + S(y)
) (* end of [add_mynat_mynat] *)

(* ****** ****** *)

implement
sub_mynat_mynat (x, y) =
(
case+ (x, y) of
| (_, Z ()) => x 
| (Z (), _) => Z () 
| (S x, S y) => x - y
) (* end of [sub_mynat_mynat] *)

(* ****** ****** *)

implement
mul_mynat_mynat (x, y) = let
//
fun loop
(
  x: mynat, y: mynat, res: mynat
) : mynat =
(
case+ x of
| Z () => res 
| S (x) => loop (x, y, res + y)
)
//
in
  loop (x, y, Z ())
end // end of [mul_mynat_mynat]

(* ****** ****** *)

implement
div_mynat_mynat (x, y) = let
//
fun loop
(
  x: mynat, y: mynat, res: mynat
) : mynat =
  if x >= y then loop (x - y, y, S(res)) else res
//
in
  loop (x, y, Z ())
end // end of [div_mynat_mynat]

(* ****** ****** *)

implement
lt_mynat_mynat (x, y) =
(
case+ (x, y) of
| (_, Z ()) => false | (Z (), _) => true | (S x, S y) => x < y
)
implement
lte_mynat_mynat (x, y) =
(
case+ (x, y) of
| (Z (), _) => true | (_, Z ()) => false | (S x, S y) => x <= y
)

(* ****** ****** *)

implement
gt_mynat_mynat (x, y) =
(
case+ (x, y) of
| (Z (), _) => false | (_, Z ()) => true | (S x, S y) => x > y
)
implement
gte_mynat_mynat (x, y) =
(
case+ (x, y) of
| (_, Z ()) => true | (Z (), _) => false | (S x, S y) => x >= y
)

(* ****** ****** *)

implement
eq_mynat_mynat (x, y) =
(
case+ (x, y) of
| (Z (), Z ()) => true | (S (x), S (y)) => x = y | (_, _) => false
)
implement
neq_mynat_mynat (x, y) =
(
case+ (x, y) of
| (Z (), Z ()) => false | (S (x), S (y)) => x != y | (_, _) => true
)

(* ****** ****** *)

implement
fact_mynat (x) =
(
case+ x of 
| Z () => S(Z()) 
| S (x1) => x * fact_mynat (x1)
)

(* ****** ****** *)
//
// Please write your code here
//
(* ****** ****** *)

implement
log2_mynat (x) = 
(
case+ x of 
| Z () => Z ()	(* It's really supposed to be INF or can I raise an exception? *)
| S (_) => 
	let
	val two = int2mynat (2) 
 	fun loop (i: mynat) : mynat = 
	  if pow_mynat_mynat (two, i) = x then i
	  else if pow_mynat_mynat (two, i) > x then i
	  else loop (i + S ( Z () ))
	in 
	  loop (Z () ) 
	end
)

implement
pow_mynat_mynat (x, y) = 
(
case+ (x, y) of
| (Z (), Z ()) => Z ()
| (S (x), Z ()) => S ( Z () )
| (Z (), S (y)) => Z ()
| (S (x1), S (y)) => x * pow_mynat_mynat (x, y)
)

(* end of [mysolution6.dats] *)
