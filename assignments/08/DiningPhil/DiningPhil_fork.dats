(* ****** ****** *)
//
// HX-2013-10-26
//
// A straightforward implementation
// of the problem of Dining Philosophers
//
(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#define ATS_DYNLOADFLAG 1

(* ****** ****** *)

staload "./mythread.sats"
staload "./DiningPhil.sats"

(* ****** ****** *)

assume fork_vtype = int

(* ****** ****** *)
//
extern
fun fork_acquire (n: int): fork
extern
fun fork_acquire2 (pf: mutex_v | n: int): fork
//
extern
fun fork_release (n: int, f: fork): void
//
(* ****** ****** *)

implement
phil_acquire_lfork (n) = f where
{
  val f = fork_acquire (phil_left(n))
  val () = println! ("Phil(", n, ") acquires left fork.")
}
implement
phil_acquire_rfork (n) = f where
{
  val f = fork_acquire (phil_right(n))
  val () = println! ("Phil(", n, ") acquires right fork.")
}

(* ****** ****** *)

implement
phil_release_lfork (n, f) = 
{
  val () = fork_release (phil_left(n), f)
  val () = println! ("Phil(", n, ") releases left fork.")
}
implement
phil_release_rfork (n, f) =
{
  val () = fork_release (phil_right(n), f)
  val () = println! ("Phil(", n, ") releases right fork.")
}

(* ****** ****** *)

staload
UN = "prelude/SATS/unsafe.sats"

(* ****** ****** *)

%{^
//
int __the_forkarr[NPHIL] ;
//
ATSinline()
atstype_ptr
the_forkarr_get () { return &__the_forkarr[0] ; }
//
%} // end of [%{^]

(* ****** ****** *)

local
//
extern
fun the_forkarr_get () : arrayref(int, NPHIL) = "ext#"

//
fun forkarr_takeout
  (n: natLt(NPHIL)): int = f where
{
  val A = the_forkarr_get ()
  val f = A[n]
  val ((*void*)) = if f != 0 then A[n] := 0
}
fun forkarr_addback
  (n: natLt(NPHIL), f: int): void =
{
  val A = the_forkarr_get ()
  val ((*void*)) = A[n] := (f)
}
//
val () = initloop (0) where
{
fun
initloop
  {i:nat} (i: int(i)): void =
  if i < NPHIL then (println! ("Initloop"); forkarr_addback (i, 1(*f*)); initloop (i+1)) else ()
// end of [initloop]
}
//
val M_forkarr = mutex_create ()
val CV_no_fork = condvar_create ()
//
in (* in of [local] *)

(* ****** ****** *)

implement
fork_acquire (n) = let
//
val (pf | ()) = mutex_lock (M_forkarr)
//
in
  fork_acquire2 (pf | n)
end // end of [fork_acquire]

implement
fork_acquire2
  (pf | n) = let
//
val n =
  $UN.cast{natLt(NPHIL)}(n)
val f = forkarr_takeout (n)
//
in
//
if f != 0
  then let
    val () = mutex_unlock (pf | M_forkarr) in f
  end // end of [then]
  else let
    val () = condvar_wait (pf | CV_no_fork, M_forkarr) in fork_acquire2 (pf | n)
  end // end of [else]
// end of [if]
//
end // end of [fork_acquire2]

implement
fork_release (n, f) = let
//
val n = $UN.cast{natLt(NPHIL)}(n)
val (pf | ()) = mutex_lock (M_forkarr)
//
val n =
  $UN.cast{natLt(NPHIL)}(n)
val f = forkarr_addback (n, f)
//
val () = condvar_signal (CV_no_fork)
//
val () = mutex_unlock (pf | M_forkarr)
//
in
  // nothing
end // end of [fork_release]

(* ****** ****** *)

end // end of [local]

(* ****** ****** *)

(* end of [DininigPhil_fork.dats] *)
