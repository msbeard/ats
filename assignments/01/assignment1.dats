//
// Word-Counting // 1st assignment of CS520
//

(* ****** ****** *)

(*
//
// Please find a detailed description at:
//
// http://www.ats-lang.org/EXAMPLE/EFFECTIVATS/word-counting/
//
*)

(* ****** ****** *)

%{^
#include "assignment1.cats"
%}

(* ****** ****** *)

#define ATS_PACKNAME "BUCASCS520.A1"

(* ****** ****** *)
//
#include "share/atspre_staload_tmpdef.hats"
//
(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"
staload _ = "libats/ML/DATS/list0.dats"

(* ****** ****** *)

extern fun word_get (): stropt = "mac#"

(* ****** ****** *)

abstype wcmap_type = ptr
typedef wcmap = wcmap_type

(* ****** ****** *)

extern fun wcmap_create (): wcmap = "mac#"
extern fun wcmap_incby1 (map: wcmap, w: string): void = "mac#"
extern fun wcmap_listize (map: wcmap): list0 @(string, int) = "mac#"

(* ****** ****** *)

extern fun WordCounting (): wcmap // implemented in ATS

(* ****** ****** *)

local

fun loop
(
  map: wcmap
) : void = let
  val opt = word_get ()
  val issome = stropt_is_some (opt)
in
//
if issome then let
  val () = wcmap_incby1 (map, stropt_unsome(opt))
in
  loop (map)
end else () // end of [if]
//
end // end of [loop]

in (* in of [local] *)

implement
WordCounting () = let
  val map = wcmap_create () in loop (map); map
end // end of [WordCounting]

end // end of [local]

(* ****** ****** *)

implement
main0 () =
{
//
val map = WordCounting ()
val wcs = wcmap_listize (map)
//
// for sorting the results
//
typedef ki = @(string, int)
//
local
fn cmp
(
  wc1: ki, wc2: ki
) :<0> int = let
  val sgn1 = compare (wc2.1, wc1.1)
in
  if sgn1 != 0 then sgn1 else compare (wc1.0, wc2.0)
end // end of [cmp]
in (*in of [local]*)
val wcs2 = list0_mergesort<ki> (wcs, lam (wc1, wc2) => cmp (wc1, wc2))
end // end of [local]
//
val nlen = list0_length (wcs2)
//
val wcs2_100 = list0_take_exn (wcs2, min (nlen, 100))
//
// for listing the top 100
// most frequently encountered words
//
local
implement
fprint_val<ki> (out, wc) = fprint! (out, wc.0, "\t->\t", wc.1)
in (*in of [local]*)
val () = fprint (stdout_ref, wcs2_100, "\n")
end // end of [local]
//
val () = fprint_newline (stdout_ref)
//
} // end of [main0]

(* ****** ****** *)

(* end of [assignment1.dats] *)
