//
// The 6th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 17th of October, 2013
//
(* ****** ****** *)

datatype
mynat = Z of () | D0 of mynat | D1 of mynat

exception NegativeMyNatExn of ()
exception DivideByZeroExn of ()

(* ****** ****** *)
//
// HX: these are implemented
//
fun mynat2int (mynat): int
fun int2mynat (i: int): mynat
//
fun fprint_mynat (out: FILEref, x: mynat): void
overload fprint with fprint_mynat
//
(* ****** ****** *)
//
// Please implement the following
//
fun add_mynat_mynat : (mynat, mynat) -> mynat // 5 points
fun sub_mynat_mynat : (mynat, mynat) -> mynat // 10 points
//
overload + with add_mynat_mynat
overload - with sub_mynat_mynat
//
(* ****** ****** *)
//
// HX: pow2(n) = 2^n
//
fun{} pow2_int (exponent: int): mynat // implementation given
//
(* ****** ****** *)
//
fun mul_mynat_int : (mynat, int(*nat*)) -> mynat // 5 points
fun div_mynat_int : (mynat, int(*nat*)) -> mynat // 5 points
fun mod_mynat_int : (mynat, int(*nat*)) -> int   // 5 points
//
overload * with mul_mynat_int
overload / with div_mynat_int
overload mod with mod_mynat_int
//
fun lt_mynat_mynat : (mynat, mynat) -> bool
fun lte_mynat_mynat : (mynat, mynat) -> bool
fun gt_mynat_mynat : (mynat, mynat) -> bool
fun gte_mynat_mynat : (mynat, mynat) -> bool
fun eq_mynat_mynat : (mynat, mynat) -> bool
fun neq_mynat_mynat : (mynat, mynat) -> bool
//
overload < with lt_mynat_mynat
overload <= with lte_mynat_mynat
overload > with gt_mynat_mynat
overload >= with gte_mynat_mynat
overload = with eq_mynat_mynat
overload != with neq_mynat_mynat
//
(* ****** ****** *)
fun{} isgtz_mynat : (mynat) -> bool // > 0 // given
fun{} digit_sum (mynat): int // sum of of digits // implementation given

(* ****** ****** *)

(* end of [assignment6-2.sats] *)
