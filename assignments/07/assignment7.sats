//
// The 7th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 24th of October, 2013
//
(* ****** ****** *)
//
#include "share/atspre_define.hats"
//
(* ****** ****** *)

staload "{$CAIRO}/SATS/cairo.sats"
staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"

(* ****** ****** *)

(* ****** ****** *)
typedef Point = @(double, double)
typedef Line = @(Point, Point)
typedef KochLines = list0 (Line)

// Given a line segment, return starting point 
fun getA (l : Line) : Point

// Given a line segment, return 1/3 distance from start
fun getB (l : Line) : Point

// Given a line segment, return middle point in Koch Curve
fun getC (l : Line) : Point

// Given a line segment, return 2/3 distance from start 
fun getD (l : Line) : Point

// Given a line segment, return end point
fun getE (l : Line) : Point

// Add two points P1, P2
fun addPoint (p1: Point, p2: Point) : Point

// Subtract two points X1, X2
fun subPoint (p1: Point, p2: Point) : Point

// Scale a Point by r 
fun mulPoint (p: Point, r: double) : Point

// Form a list of points to draw the Koch Curve
fun kochList (l : Line) : KochLines

// Print Points
fun fprint_point (out: FILEref, P: Point): void

// Draw the lines
fun render{l:agz} (cr: !cairo_ref(l), K: KochLines): void

// KochGen 
fun kochGen (K: KochLines) : KochLines

// Koch Entry 
fun koch{l:agz} (cr: !cairo_ref(l), iter: int, entry: KochLines) : void 
(* ****** ****** *)

//
// HX: 20 points +
//     some quality-based bonus points
//
fun mydraw{l:agz} ( !cairo_ref(l), width: int, height: int, ntime: int ) : void = "ext#assignment7_mydraw"

(* ****** ****** *)

(* end of [assignment7.sats] *)
