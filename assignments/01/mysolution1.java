//
// Student Name: Michelle Beard
// Student BUID: U17422789
//

/* ****** ****** */
/*
 (*
 //
 // Please find a detailed description at:
 //
 // http://www.ats-lang.org/EXAMPLE/EFFECTIVATS/word-counting/
 //
 *)
 */
/* ****** ****** */
//
// Please put your code in this file
//
/* ****** ****** */
import java.util.*;
import java.io.*;
import java.util.List;
import java.util.Map.*;

/**
 * stringANDint A key-value pair implementation
 *
 * @author msbeard
 * @param <String> The key
 * @param <Integer> The value
 */
class stringANDint<String, Integer>
{

    final private String mKey;
    private Integer mVal;

    stringANDint(String key, Integer val)
    {
        this.mKey = key;
        this.mVal = val;
    }

    public String getKey()
    {
        return this.mKey;
    }

    public Integer getValue()
    {
        return this.mVal;
    }

    public void setValue(Integer val)
    {
        this.mVal = val;
    }

    @Override
    public java.lang.String toString()
    {
        return this.mKey + " ==> " + this.mVal.toString();
    }
}

/**
 * The WordCount class counts the number of words in a text document
 */
public class mysolution1
{

    String[] mLines;
    private int mCurr;

    /**
     * Create an empty map
     *
     * @return A Map<String, Integer>
     */
    private static Map<String, Integer> wcmap_create()
    {
        Map<String, Integer> hm = new HashMap<String, Integer>();
        return hm;
    }

    /**
     * Convert all lines to lowercase and remove all non-alpha characters
     *
     * @param line
     * @return A cleaned up string
     */
    private static String cleanup(String line)
    {
        line = line.toLowerCase();
        line = line.replaceAll("[^a-z]", " ");
        line = line.trim();
        return line;
    }

    private void setWords(String[] words)
    {
        mLines = words;
    }

    /**
     * Retrieve a word
     *
     * @return
     */
    private String word_get()
    {
        return mLines[mCurr];
    }

    /**
     * Given a map and a string, increment the counter by 1 iff the str exists
     * in the map
     *
     * @param map The map representation
     * @param str A string
     */
    private static void wcmap_incby1(Map<String, Integer> map, String str)
    {
        int counter = 1;
        if (map.containsKey(str))
        {
            map.put(str, map.get(str) + 1);
        }
        else
        {
            map.put(str, counter);
        }
    }

    /**
     * Convert the map to a list representation where all elements are sorted by
     * their count
     *
     * @param map The map representation
     * @return A sorted list representation List(<string, integer>)
     */
    private static List<stringANDint<String, Integer>> wcmap_listize(Map<String, Integer> map)
    {
        List<stringANDint<String, Integer>> lst;
        lst = new ArrayList<stringANDint<String, Integer>>();

        // Iterate over map key,val pairs and create stringANDint objects
        // and insert into list
        for (Entry<String, Integer> e : map.entrySet())
        {
            stringANDint<String, Integer> obj;
            obj = new stringANDint<String, Integer>(e.getKey(), e.getValue());
            lst.add(obj);
        }

        // Sort entries by value
        Collections.sort(lst, new Comparator<stringANDint<String, Integer>>()
        {
            public int compare(stringANDint<String, Integer> op1,
                    stringANDint<String, Integer> op2)
            {
                return op2.getValue().compareTo(op1.getValue());
            }
        });

        return lst;
    }

    private static void printTop(List<stringANDint<String, Integer>> lst, int len)
    {
        for (int i = 0; i < len; i++)
        {
            System.out.println(lst.get(i).toString());
        }
    }

    /**
     * Return the smallest input
     *
     * @param op1 The first input
     * @param op2 The second input
     * @return The smallest input
     */
    private static int min(int op1, int op2)
    {
        if (op1 > op2)
        {
            return op2;
        }
        else
        {
            return op1;
        }
    }

    private void WordCounting() throws IOException
    {
        BufferedReader inputStream = null;
        Map<String, Integer> hm = wcmap_create();
        try
        {
            inputStream = new BufferedReader(new InputStreamReader(System.in));
            String line;
            while ((line = inputStream.readLine()) != null)
            {
                line = cleanup(line);
                if (!line.trim().isEmpty())
                {
                    setWords(line.split("\\s+"));
                    for (mCurr = 0; mCurr < mLines.length; mCurr++)
                    {
                        String w = word_get();
                        wcmap_incby1(hm, w);
                    }
                }
            }
        }
        finally
        {
            if (inputStream != null)
            {
                inputStream.close();
            }
        }
        List<stringANDint<String, Integer>> list = wcmap_listize(hm);

        int elem = min(100, list.size());
        printTop(list, elem);
    }

    public static void main(String args[]) throws IOException
    {
        mysolution1 m = new mysolution1();
        m.WordCounting();
    }
}
/* end of [mysolution1.java] */
