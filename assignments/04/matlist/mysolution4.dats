//
// The 4th assignment
// BU CAS CS520, Fall, 2013
//
(* ****** ****** *)
//
// Solution to Assignment 4
//
(* ****** ****** *)
//
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

staload "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"
staload _ = "libats/ML/DATS/list0.dats"

(* ****** ****** *)

staload "./assignment4.sats"

(* ****** ****** *)

#define ATS_DYNLOADFLAG 0 // no dynloading at runtime

(* ****** ****** *)

fun itoa (i: int): string = strptr2string(g0int2string(i))

(* ****** ****** *)
//
// HX:
// these two functions return the first column
// and the rest of the columns of a given matlist,
// respectively
// 
//
extern
fun{
a:t@ype
} matlist_hcol (xss: list0(list0(a))): list0(a)

extern
fun{
a:t@ype
} matlist_tcol (xss: list0(list0(a))): list0(list0(a))

(* ****** ****** *)

//MB: Didn't really need this for the assignment
implement{a}
matlist_get_nrow (M) = 
(
case+ M of 
| list0_nil () => 0
| list0_cons (_, xss) => 1 + matlist_get_nrow<a> (xss) 
) (* end of [matlist_get_nrow] *)

implement{a}
matlist_hcol (M: matlist(a)) : list0(a) = 
(
case+ M of 
| list0_nil () => list0_nil ()
| list0_cons (xs, xss) => list0_cons{a}( xs[0],  matlist_hcol<a> (xss) )
) (* end of [matlist_hcol] *)

implement{a}
matlist_tcol (M: matlist(a)) : matlist(a) = 
(
case+ M of 
| list0_nil () => list0_nil ()
| list0_cons (xs, xss) => list0_cons{list0(a)} (list0_tail_exn<a> (xs), matlist_tcol<a> (xss))
)

implement{a}
fprint_matlist (out: FILEref, M: matlist(a)) = 
(
case+ M of 
| list0_nil () => ()
| list0_cons (xs, xss) => (fprint_list0_sep (out, xs, ", "); fprint_newline (out); fprint_matlist (out, xss))
)

(*
HX version
implement{a}
matlist_get_row (M, row) =
let  
  val-cons0 (xs, M) = M
in
  if row > 0 then matlist_get_row<a> (M, row-1) else xs
end // end of [matlist_get_row] 
*)

implement{a}
matlist_get_row (M, row) = 
let
  fun loop (i: int, xss: matlist(a)) : list0(a) =  
    if i <= row then
      let 
      // val () = println! ("i = ", i)
      in
      case+ xss of 
      | list0_cons (xs, xss) => 
	if i = row then 
 	  let
	    // val () = println! ("Row and i are equal, i = ", i)
   	  in
	    xs
	  end
        else loop (succ (i), xss)
      | list0_nil () => list0_nil ()
      end
    else $raise MatrixSubscriptExn()
in
  loop (0, M)
end

implement{a}
matlist_get_at (M, i, j) = 
let
  val xs = matlist_get_row<a> (M, i)
  val numCol = list0_length (xs)
in
  if j >= numCol then $raise MatrixSubscriptExn() else xs[j] 
end (* end of [matlist_get_at] *)

(*
implement{a}
matlist_get_at (M, i, j) = 
try
  let
    val xs = matlist_get_row<a> (M, i)
  in
    xs[j]
  end
with
|~ListSubscriptExn () => $raise MatrixSubscriptExn() (* end of [matlist_get_at] *)
*)

(* 
// MB: Another way described in class
// Specification below: Wont compile but
// specification is accurate. 
implement{a}
matlist_get_at (M, i, j) =
try
  let 
    val xs = M[i]
  in
    xs[j]
  end
with
|~ListSubscriptExn () => $raise MatrixSubscriptExn() (* end of [matlist_get_at] *)
*)

implement{a}
matlist_transpose (xss, nrow, ncol) =
(
  if ncol > 0 then let
    val hcol = matlist_hcol<a> (xss)
    val tcol = matlist_tcol<a> (xss)
  in
    cons0{list0(a)}(hcol, matlist_transpose<a> (tcol, nrow, ncol-1))
  end else nil0() // end of [if]
)

(* ****** ****** *)

(* end of [mysolution4.dats] *)
