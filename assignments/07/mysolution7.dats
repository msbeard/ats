//
// The 7th assignment
// BU CAS CS520, Fall, 2013
// Due by Thursday, the 24th of October, 2013
//
(* ****** ****** *)
//
#include
"share/atspre_define.hats"
#include
"share/atspre_staload.hats"
//
(* ****** ****** *)

#define PI 3.1415926535898

(* ****** ****** *)

staload "libc/SATS/math.sats"
staload _ = "libc/DATS/math.dats"
staload "libats/ML/SATS/basis.sats"
staload _ = "libats/ML/SATS/basis.sats"
staload "libats/ML/SATS/list0.sats"
staload _ = "libats/ML/SATS/list0.sats"
staload _ = "libats/ML/DATS/list0.dats"
staload STDLIB = "libc/SATS/stdlib.sats"
(* ****** ****** *)

staload "./assignment7.sats"

(* ****** ****** *)

staload "{$CAIRO}/SATS/cairo.sats"

(* ****** ****** *)

(* ****** ****** *)
//
// Please write your code here
//
(* ****** ****** *)

// Takes a while to render, but produces 
// (not exact) representation of Sierbenski Triangle
extern fun 
drawCircle{l:agz} (!cairo_ref(l), width: double, height: double, r: double) : void

implement
drawCircle{l} (cr, W, H, R) = 
let
  val () = cairo_set_source_rgb (cr, 0.0, 0.0, 0.0)
  val () = cairo_new_sub_path (cr)
  val () = cairo_arc (cr, W, H, R, 0.0, 2 * PI)
  val () = cairo_stroke_preserve (cr)
in
  if R > 4.0 then 
  let 
    val () = drawCircle (cr, W+R, H, R/2)
    val () = drawCircle (cr, W-R, H, R/2)
    val () = drawCircle (cr, W, H+R, R/2)
  in
    // nothing
  end  
end // end of [drawCircle] 

// Cantor Set
extern fun 
cantorSet{l:agz} (!cairo_ref(l), x: double, y: double, wid: double, H: double) : void

implement 
cantorSet{l} (cr, x, y, wid, H) =
let
  #define SH 10.0 
  val () = cairo_set_source_rgb (cr, 0.0, 0.0, 0.0)
  val () = cairo_rectangle (cr, x, y, wid, 3.0)
  val () = cairo_fill (cr)
in
  if wid > 1.0 || y > H - 10 then 
  let
    val () = cantorSet (cr, x, y + SH, wid/3, H)
    val () = cantorSet (cr, x + wid * 2/3, y + SH, wid/3, H) 
  in
    // nothing
  end
end // end of [cantorSet] 

implement addPoint (p1, p2) = 
(p1.0+p2.0, p1.1+p2.1)
(* end of [addPoint] *)

implement subPoint (p1, p2) = 
(p1.0-p2.0, p1.1-p2.1)
(* end of [subPoint] *)

implement mulPoint (p, r) =
(p.0*r, p.1*r)
(* end of [mulPoint] *)

implement getA (l) =
let
  // nothing
in
 l.0
end (* end of [getA] *)

implement getB (l) = 
let
  val start = getA (l);
  val last = getE (l);
  val diff = subPoint (last, start);
in
  addPoint (start, mulPoint (diff, 1.0/3.0))
end (* end of [getB] *)

implement getC (l) = 
let
  val a = getA (l);
  val e = getE (l);
  val dBP = subPoint (e, a);
  val dBP = mulPoint (dBP, 1.0/3.0);
  val P = addPoint (a, dBP);
  val th = ~PI/3.0 // 60 degrees
  val x = (cos (th) * dBP.0) - (sin (th) * dBP.1)
  val y = (sin (th) * dBP.0) + (cos (th) * dBP.1)   
in
  addPoint(P, (x, y))
end (* end of [getC] *)

implement getD (l) = 
let
  val start = getA (l);
  val last = getE (l);
  val diff = subPoint (start, last);
in
  addPoint (last, mulPoint (diff, 1.0/3.0))
end (* end of [getD] *)

implement getE (l) = 
let
// nothing
in
  l.1
end (* end of [getE] *)

implement kochList (l) =
let
  // Needs to be in the let expression
  macdef ::(x, xs) = list0_cons{Line}(,(x), ,(xs))
  val a = getA (l);
  val b = getB (l); 
  val c = getC (l);
  val d = getD (l);
  val e = getE (l);
  val AB = (a, b);
  val BC = (b, c);
  val CD = (c, d);
  val DE = (d, e);
  val res = AB :: BC :: CD :: DE :: list0_nil ()
in
  res
end (* end of kochList *)

implement fprint_point (out, P) = 
let
// nothing
in
  fprint! (out, "(", P.0, ", ", P.1, ")\n")
end (* end of [fprint_point] *)

implement render (cr, xs) = 
(
case+ xs of 
| list0_nil () => () 
| list0_cons (x, xs) => 
  let
    val r = $STDLIB.drand48 ()
    and g = $STDLIB.drand48 ()
    and b = $STDLIB.drand48 ()
    val () = cairo_set_source_rgb (cr, r, g, b) // black
    val () = cairo_move_to (cr, x.0.0, x.0.1)
    val () = cairo_line_to (cr, x.1.0, x.1.1)
    val () = cairo_stroke (cr)
  in
    render (cr, xs)
  end
) (* end of [render] *)

implement koch (cr, iter, startLine) = 
let
  val nextGen = 
  let
    fun loop (i : int, K: KochLines) : KochLines = 
      if i < iter then 
 	loop (i+1 , kochGen (K))
      else K
  in
    loop (0, startLine)
  end
in
  render (cr, nextGen)
end (* end of [koch] *)

implement kochGen (K) = 
case+ K of 
| list0_nil () => list0_nil ()
| list0_cons (k, K) => list0_append<Line> (kochList (k), kochGen (K))
(* end of [kochGen] *)

extern fun 
distance (p1: Point, p2: Point) : double

extern fun
midpoint (p1: Point, p2: Point) : Point

implement 
distance (p1, p2) = 
let
  val dx = p2.0 - p1.0 and dy = p2.1 - p1.1
  val dx = dx * dx and dy = dy * dy
in
 sqrt (dx+dy)
end (* end of [distance] *)

implement
midpoint (p1, p2) =
let
  val dx = p1.0 + p2.0 and dy = p1.1 + p2.1
  
in
  (dx/2, dy/2)
end (* end of [midpoint] *)

implement
mydraw{l} (cr, W, H, n) =
let
  macdef ::(x, xs) = list0_cons{Line}(,(x), ,(xs))
  val W = g0int2float_int_double (W);
  val H = g0int2float_int_double (H);
  val (pf | () ) = cairo_save (cr)
  // val () = drawCircle (cr, W/2, H/2, H/4)
  // val () = cantorSet (cr, 0.0, H/3, W, H)
  
  // To draw the Koch Snowflake, need first line segment
  val a = (W/6, H/4)
  val b = (W-W/6, H/4)
  val d = distance (a, b)
  val m = midpoint (a, b)
  val c = (m.0, m.1 + d)
  val A = (a, b)
  val B = (b, c)
  val C = (c, a)
  val startLine = A :: B :: C :: list0_nil ()
  val maxIter = 7
  val iter = n mod maxIter
  val () = koch (cr, iter, startLine)
  val () = cairo_restore (pf | cr)
in
  // nothing
end // end of [mydraw]
(* end of [mysolution7.dats] *)
